插件选择：Prettier-Code formatter
# 命令
```cs
//将Soft2ApiDemo.Domain项目添加到解决方案中
dotnet sln add .\Soft2ApiDemo.Domain\
//将Soft2ApiDemo.Infrastructure项目添加到解决方案中
dotnet sln add .\Soft2ApiDemo.Infrastructure\
//进入
cd .\Soft2ApiDemo.Infrastructure\
//安装包(-v 是指定版本)
dotnet add package Microsoft.EntityFrameworkCore -v 3.1
dotnet add package Microsoft.EntityFrameworkCore.SqlServer -v 3.1
//进入
cd .\Soft2ApiDemo.Api\
//安装包
dotnet add package Microsoft.EntityFrameworkCore.Design -v 3.1
//向本地工具清单中添加ef
dotnet tool install -g dotnet-ef
//向当前目录中的项目添加多个项目引用
dotnet add ..\Soft2ApiDemo.Api\ reference ..\Soft2ApiDemo.Infrastructure\
dotnet add ..\Soft2ApiDemo.Infrastructure\ reference ..\Soft2ApiDemo.Domain\
//生成项目及其所有依赖项(类似检查错误)
dotnet build
```
类库
Soft2ApiDemo.Domain文件夹下Entity文件夹下Roles.cs
```cs
namespace Soft2ApiDemo.Domain.Entity
{
    // 角色
    public class Roles : BaseEntity
    {
        /// 角色名称
        public string Rolename { get; set; }
    }
}
```
Soft2ApiDemo.Domain文件夹下Entity文件夹下UserRoles.cs
```cs
using System;

namespace Soft2ApiDemo.Domain.Entity
{
    /// 用户角色实体类型
    public class UserRoles : BaseEntity
    {
        ///用户Id
        public Guid UserId { get; set; }

        ///角色Id
        public Guid RoleId { get; set; }

        /// 用户实例
        public Users User { get; set; }

        ///角色实例
        public Roles Role { get; set; }
    }
}
```
Soft2ApiDemo.Domain文件夹下Entity文件夹下Users.cs
```cs
namespace Soft2ApiDemo.Domain.Entity
{
    public class Users : BaseEntity
    {
        public string Username { get; set; }

        public string Password { get; set; }

    }
}
```

Soft2ApiDemo.Domain文件夹下BaseEntity.cs
```cs
using System;

namespace Soft2ApiDemo.Domain
{
    // 所有实体类型的公共属性/字段
    public abstract class BaseEntity
    {
        // 主键
        public Guid Id { get; set; }

        // 标识是否启用
        public bool IsActived { get; set; }

        // 是否删除（伪删除、软删除）
        public bool IsDeleted { get; set; }

        // 创建者（的Id）
        public Guid CreatedBy { get; set; }

        // （最后）修改者（的Id）
        public Guid UpdatedBy { get; set; }

        // 记录的创建时间
        public DateTime CreatedAt { get; set; }

        // （最后）更新时间
        public DateTime UpdatedAt { get; set; }

        // 展示顺序，默认为0
        public int DisplayOrder { get; set; }

        // 备注
        public string Remarks { get; set; }
    }
}
```
类库
Soft2ApiDemo.Infrastructure文件下Db文件下Soft2ApiDemoDbContext.cs
```cs
using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Domain.Entity;

namespace Soft2ApiDemo.Infrastructure.Db
{
    public class Soft2ApiDemoDbContext : DbContext
    {
        private readonly string
            _conString = "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;";

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer (_conString);
        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }

        public DbSet<UserRoles> UserRoles { get; set; }
    }
}
```
# 命令
```cs
/*
生成迁移文件指令
基础指令
dotnet ef migrations add 名称
指定项目
dotnet ef migrations add 名称 --project目标项目
指定项目和启动项目(-s就是--startup-project)
dotnet ef migrations add 名称 --project目标项目 --startup-project启动项目
*/
//要在Soft2ApiDemo.Infrastructure
dotnet ef migrations add 88 -s ..\Soft2ApiDemo.Api\
//向当前目录中的项目添加多个项目引用
dotnet add reference ..\Soft2ApiDemo.Domain\
//进行数据库迁移
/*
需要打开SqlServer服务
搜索windows服务，找到SQL Server (MSSQLSERVER)打开
*/
dotnet ef database update -s ..\Soft2ApiDemo.Api\
```



