# 命令
```cs
dotnet ef migrations add addlog --project .\Soft2ApiDemo.Infrastructure\ -s .\Soft2ApiDemo.Api\
dotnet ef database update --project .\Soft2ApiDemo.Infrastructure\ -s .\Soft2ApiDemo.Api\
dotnet run --product .\Soft2ApiDemo.Api\
dotnet add .\Soft2ApiDemo.Infrastructure\ package Serilog.Sinks.MSSqlServer
//dotnet clean- 清除项目输出
```
Soft2ApiDemo.Domain文件夹下Entity文件夹下AppLog.cs
```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft2ApiDemo.Domain.Entity
{
    public class AppLog:BaseEntity
    {
        public string JsonLog{get;set;}
    }
}
```
Soft2ApiDemo.Infrastructure文件夹下Db文件夹下Soft2ApiDemoDbContext.cs
```cs
using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Domain.Entity;

namespace Soft2ApiDemo.Infrastructure.Db
{
    public class Soft2ApiDemoDbContext : DbContext
    {
        public Soft2ApiDemoDbContext(DbContextOptions options) :
            base(options)
        {
        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }

        public DbSet<UserRoles> UserRoles { get; set; }

        public DbSet<AppLog> AppLog { get; set; }
    }
}
```
Soft2ApiDemo.Api文件夹下Controllers文件夹下UsersController.cs
```cs
using System;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Soft2ApiDemo.Domain.Entity;
using Soft2ApiDemo.Domain.Repository;
using Soft2ApiDemo.Infrastructure.Utils;

namespace Soft2ApiDemo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IRepositoryBase<Users> _usersRes;
        private readonly IRepositoryBase<AppLog> _logRes;

        public UsersController(
            IRepositoryBase<Users> usersRes,
            ILogger<UsersController> logger,
            IRepositoryBase<AppLog> logRes
            )
        {
            _logger=logger;
            _usersRes = usersRes;
            _logRes = logRes;
        }

        [HttpGet]
        public IEnumerable<dynamic> Index()
        {
            _usersRes
                .Add(new Users {
                    Username = "老胡来也",
                    Password = "没有密码"
                });
            var lst =
                _usersRes
                    .Table
                    .Select(x =>
                        new {
                            x.Id,
                            x.Username,
                            x.Password,
                            Nickname = "柯文龙不要睡觉"
                        })
                    .ToArray();
            var uuu =
                new List<dynamic> {
                    new {
                        Name = "天天向上",
                        Age = "3389",
                        Sex = "男",
                        Children =
                            new List<dynamic> {
                                new { KidName = "大娃", Age = 1 },
                                new { KidName = "二娃", Age = 0.9 }
                            }
                    },
                    new {
                        Nickname = "你的童年我的童年，好像都一样",
                        Height = 1990
                    }
                };
                //_logRes.Add(new AppLog{JsonLog=uuu.SerializeObject()});
                //_logger.LogInformation( uuu.SerializeObject());
                _logger.LogInformation("Mssage");
            return uuu;
        }

        [HttpPost]
        public Users AddUser(UserCto model)
        {
            var tmp =
                new Users {
                    Username = model.Username,
                    Password = model.Password
                };
            _usersRes.Add (tmp);
            return tmp;
        }

        public class UserCto
        {
            public string Username { get; set; }

            public string Password { get; set; }
        }

        [HttpPut]
        [Route("{id}")]
        public Users UpdateUser(Guid id, UserCto model)
        {
            var entity = _usersRes.GetById(id);
            if (entity != null)
            {
                entity.Password = model.Password;
                _usersRes.Update (entity);
            }
            return entity;
        }

        // [HttpDelete]
        [Route("{id}")]
        public dynamic DeleteUser(Guid id)
        {
            _usersRes.Delete (id);
            return new { Code = 1000, Msg = "删除成功", Data = "" };
        }

        [HttpPost]
        [Route("{id}/friends")]
        public dynamic GetFriends(Guid id, UserCto model)
        {
            var lst =
                new {
                    Code = 1000,
                    Msg = "为指定用户新增朋友成功",
                    Data = new { Id = id, Model = model }
                };
            return lst;
        }

        [Route("{id}/friends")]
        public IEnumerable<dynamic> GetFriends(Guid id)
        {
            var lst =
                new List<dynamic> {
                    new { Name = "唐大腿", Age = 16 },
                    new { Name = "王文文", Age = 28 }
                };

            return lst;
        }

        [Route("{id}/friends/{friendId}")]
        public dynamic GetFriends(Guid id, Guid friendId)
        {
            var lst =
                new {
                    Code = 1000,
                    Msg = "成功",
                    Data = new { Name = "唐大腿", Age = 16 }
                };

            return lst;
        }
    }
}

```
Soft2ApiDemo.Api文件夹下Program.cs
```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Events.MSSqlServer;

namespace Soft2ApiDemo.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger =
                new LoggerConfiguration()
                    .WriteTo
                    .MSSqlServer(connectionString:"Server=.;Database=Soft2ApiDemo;uid=sa;pwd=123456;",
                    sinkOptions:new MSSqlServerSinkOptions{
                        TableName="LogEvents"
                    })
                    .CreateLogger();

            try
            {
                Log.Information("开始Web宿主");
                CreateHostBuilder(args).Build().Run();
                // return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                // return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
            // CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
                .CreateDefaultBuilder(args)
                .UseSerilog((context, services, configuration) =>
                    configuration
                        .ReadFrom
                        .Configuration("Serilog")
                        .ReadFrom
                        .Services(services)
                        .Enrich
                        .FromLogContext()
                        .WriteTo
                        .Console())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

```


