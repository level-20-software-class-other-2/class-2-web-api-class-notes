# 创建项目模板

 ## 配置文件
 ~~~~
创建一个文件夹(temple.config)在该文件夹内创建一个template.json的文件开始配置
 ~~~~

 ## 配置文件内容 
 ~~~~cs
{
    "$schema": "http://json.schemastore.org/template",//template.json 文件的 JSON 架构，可以不要该键值对
    "author": "menglingkun", //必填！模板作者
    "classifications": [ "Web/WebAPI" ], //必填，这个对应模板的Tags，其他的比如 [ "Common", "Console" ],
    "name": "Core.Webapi.Template", //必填，这个是模板名，比如ASP.NET Core Web API
    "identity": "Core.Webapi.Template.HGD", //可选，模板的唯一名称
    "shortName": "corewebapi", //必填，这个对应模板的短名称，比如webapi
    "tags": {
      "language": "C#" ,
      "type":"project"
    },
    "sourceName": "SoftDemo",  // 可选，要替换的名字，这个就是模板的项目名，以后新建的时候，会把这个名字替换成其他，比如HelloBlog（警告！这里不要写一些专用词汇，比如app呀，net呀，core之类的）
    "preferNameDirectory": true  // 可选，添加目录  
}
 ~~~~

## 命令
 ~~~
 dotnet new -i (所创建包的名称)
 dotnet new -l //查看是否安装成功
 ~~~

# 打包nupkg
 ~~~cs
<?xml version="1.0" encoding="utf-8"?>
<package xmlns="http://schemas.microsoft.com/packaging/2012/06/nuspec.xsd">
  <metadata>
    <id>MyWebApi.template.HGD</id> //这里是自己命名的。自行创建的话，要重新换一个ID
    <version>1.0.0</version>
    <description>
      Creates a core webapi app.
    </description>
    <authors>menglingkun</authors>
    <packageTypes>
      <packageType name="Template" />
    </packageTypes>
    <license type="file">license\license.txt</license>
  </metadata>
</package>
 ~~~

## 下载nuget.exe文件
~~~
 下载Nuget.exe：https://www.nuget.org/downloads 把下载好的 nuget.exe 
~~~

## 出现的权限问题
 ~~~
把下载好的nuget.exe文件放在同级目录中
使用命令：nuget pack (是上面刚创建好的文件地址后缀是.nuspec)
在这里如果直接使用打包命令会出现报错，这里的报错是权限问题，
解决方案：
1、添加环境变量，F:\temple (这里是我自己放的地方如果你们要设置的话一定要找到文件的正确路劲)
2、直接把文件放在C:\Program Files\dotnet里面。

问题就解决
 ~~~

## 上传到nuget.org
 ~~~
1、
地址：https://www.nuget.org/packages/manage/upload
这里如果你有微软账户直接登录，如果没有的话要自己创建账户；
2、
登录之后点击Upload,进入上传页面，点击Browse上传已生成的包，在这里网页会直接判断你所上传的包合格或不合格，
如果显示包的ID错误，就要修改.nuspec里面的XML文件ID，
如果合格：会直接上传然后给你的电子邮件发文件，到这里就已经完成一大半了，
 ~~~
