using System;
using System.Linq;
namespace WebapiDemo.Domain.Repository
{
    public interface IRepositoryBase<T>
    {
        T GetById (Guid id);
        IQueryable<T> Table { get; }
        void Add(T entity);
        void Update(T entity);
        void Delete(Guid id);


    }
}