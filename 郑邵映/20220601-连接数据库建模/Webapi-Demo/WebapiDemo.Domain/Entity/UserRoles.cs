using System;
namespace WebapiDemo.Domain.Entity
{
    public class UserRoles : BaseEntity
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public Users User { get; set; }
        public Roles Role { get; set; }

    }
}