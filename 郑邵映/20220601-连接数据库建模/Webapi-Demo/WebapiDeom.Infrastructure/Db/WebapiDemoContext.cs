using Microsoft.EntityFrameworkCore;
using WebapiDemo.Domain.Entity;
namespace WebapiDemo.Infrastructure.Db
{
    public class WebapiDemoContext : DbContext
    {
        private readonly string constring = "server=.;database=WebapiDemo;uid=sa;pwd=123456";

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(constring);
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
    }
}