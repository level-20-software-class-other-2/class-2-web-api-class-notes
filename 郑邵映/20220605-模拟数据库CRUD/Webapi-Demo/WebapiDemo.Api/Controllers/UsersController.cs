using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebapiDemo.Domain.Entity;
using WebapiDemo.Domain.Repository;

namespace WebapiDemo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IRepositoryBase<Users> _usersRes;
        public UserController(IRepositoryBase<Users> usersRes)
        {
            _usersRes = usersRes;
        }

        [HttpGet]
        public IEnumerable<Users> Index()
        {
            _usersRes.Add(new Users
            {
                UserName = "aaa",
                Password = "123"
            });
            var lst = _usersRes.Table.ToArray();
            return lst;
        }
        [HttpPost]
        public Users AddUser(UserCto model)
        {
            var tmp = new Users
            {
                UserName = model.UserName,
                Password = model.Password
            };
            _usersRes.Add(tmp);
            return tmp;
        }

        public class UserCto
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }
        [HttpPut]
        [Route("{id}")]
        public Users UpdateUser(Guid id, UserCto model)
        {
            var entity = _usersRes.GetById(id);
            if (entity != null)
            {
                entity.Password = model.Password;
                _usersRes.Update(entity);
            }
            return entity;
        }
        [HttpDelete]
        [Route("{id}")]
        public dynamic DeleteUser(Guid id)
        {
            _usersRes.Delete(id);
            return new
            {
                code = 1000,
                msg = "删除成功",
                Data = ""
            };
        }
    }
}