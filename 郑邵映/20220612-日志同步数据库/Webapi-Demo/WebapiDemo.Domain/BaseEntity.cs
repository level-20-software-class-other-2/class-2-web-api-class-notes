﻿using System;

namespace WebapiDemo.Domain
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
        public Guid CreatedBy{get;set;}
        public Guid UpdatedBy{get;set;}
        public DateTime CreateDAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int DisplayOrder { get; set; }
        public string Remarks{get;set;}
    }
}
