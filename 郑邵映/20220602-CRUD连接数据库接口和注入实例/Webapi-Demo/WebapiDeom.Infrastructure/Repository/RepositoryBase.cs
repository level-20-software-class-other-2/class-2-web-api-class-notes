using System;
using System.Linq;
using WebapiDemo.Domain;
using Microsoft.EntityFrameworkCore;
using WebapiDemo.Domain.Repository;
using WebapiDemo.Infrastructure.Db;

namespace WebapiDemo.Infrastructure.Repository
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : BaseEntity
    {
        private readonly WebapiDemoContext _db;
        private DbSet<T> _table;
        public RepositoryBase(WebapiDemoContext db)
        {
            _db = db;
            _table = _db.Set<T>();
        }

        public T GetById(Guid id)
        {
            var entity = _table.Where(x => x.Id == id).FirstOrDefault();
            return entity;
        }

        public IQueryable<T> Table
        {
            get
            {
                if (_table == null)
                {
                    _table = _db.Set<T>();
                }
                return _table.AsNoTracking().AsQueryable<T>();
            }
        }
        public void Add(T entity)
        {
            _db.Add(entity);
            _db.SaveChanges();
        }

        public void Delete(Guid id)
        {
            _db.Remove(id);
            _db.SaveChanges();
        }

        public void Update(T entity)
        {
            _db.Update(entity);
            _db.SaveChanges();
        }
    }
}