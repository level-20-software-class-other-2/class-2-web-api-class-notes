//编码序列化
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Encodings.Web;
using System.Text.Unicode;
namespace WebapiDeom.Infrastructure.Utils
{
    public static class JsonHelper
    {
        public static string SerializeObject(this object obj)
        {
            var options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
                WriteIndented = true
            };
            return JsonSerializer.Serialize(obj,options);
        }
    }
}