﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebapiDemo.Domain.Entity;
using WebapiDemo.Domain.Repository;
namespace WebapiDemo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IRepositoryBase<Users> _usersTable;
        public WeatherForecastController(ILogger<WeatherForecastController> logger,IRepositoryBase<Users> usersTable)
        {
            _logger = logger;
            _usersTable = usersTable;
        }

        [HttpGet]

        public IEnumerable<WeatherForecast> Get()
        {

            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
        [Route("[controller]/xx")]
        public IEnumerable<dynamic> uu()
        {
            var lst = _usersTable.Table.ToArray();
            return lst;
        }
    }
}
