namespace Soft2ApiDemo.Domain.Entity
{
    // 角色
    public class Roles : BaseEntity
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        /// <value></value>
        public string Rolename { get; set; }
    }
}
