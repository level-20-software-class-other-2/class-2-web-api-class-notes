namespace Soft2ApiDemo.Domain.Entity
{
    public class AppLogs : BaseEntity
    {
        public string Message { get; set; }

        public string Exception { get; set; }

    }
}
