using System;
using System.Linq;

namespace Soft2ApiDemo.Domain.Repository
{
    /// <summary>
    /// CRUD接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepositoryBase<T>
    {
        T GetById(Guid id);

        IQueryable<T> Table { get; }

        void Add(T entity);

        void Update(T entity);

        void Delete(Guid id);
    }
}
