using System;

namespace Soft2ApiDemo.Domain
{
    // 所有实体类型的公共属性/字段
    public abstract class BaseEntity
    {
        // 主键
        public Guid Id { get; set; }

        // 标识是否启用
        public bool IsActived { get; set; }

        // 是否删除（伪删除、软删除）
        public bool IsDeleted { get; set; }

        // 创建者（的Id）
        public Guid CreatedBy { get; set; }

        // （最后）修改者（的Id）
        public Guid UpdatedBy { get; set; }

        // 记录的创建时间
        public DateTime CreatedAt { get; set; }

        // （最后）更新时间
        public DateTime UpdatedAt { get; set; }

        // 展示顺序，默认为0
        public int DisplayOrder { get; set; }

        // 备注
        public string Remarks { get; set; }
    }
}
