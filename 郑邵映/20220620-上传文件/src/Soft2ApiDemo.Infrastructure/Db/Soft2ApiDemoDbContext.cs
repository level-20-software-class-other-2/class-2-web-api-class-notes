using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Domain.Entity;

namespace Soft2ApiDemo.Infrastructure.Db
{
    public class Soft2ApiDemoDbContext : DbContext
    {
        public Soft2ApiDemoDbContext(DbContextOptions options) :
            base(options)
        {
        }

        // private readonly string
        //     _conString = "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;";

        // protected override void OnConfiguring(DbContextOptionsBuilder builder)
        // {
        //     builder.UseSqlServer (_conString);
        // }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }

        public DbSet<UserRoles> UserRoles { get; set; }
        public DbSet<AppLogs> AppLogs { get; set; }
        public DbSet<AuditLog> AuditLog { get; set; }
    }
}
