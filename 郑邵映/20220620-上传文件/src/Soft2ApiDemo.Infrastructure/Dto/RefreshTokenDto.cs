using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft2ApiDemo.Infrastructure.Dto
{
    public class RefreshTokenDto
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}