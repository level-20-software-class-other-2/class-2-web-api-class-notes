using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace Soft2ApiDemo.Infrastructure.Utils
{
    public static class JsonHelper
    {
        public static string SerializeJson(this object obj)
        {
            var options =
                new JsonSerializerOptions {
                    Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
                    WriteIndented = true
                };
            return JsonSerializer.Serialize(obj, options);
        }
    }
}
