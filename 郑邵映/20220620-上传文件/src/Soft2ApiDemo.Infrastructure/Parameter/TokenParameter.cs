using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft2ApiDemo.Infrastructure.Parameter
{
    /// <summary>
    /// Token的配置相对的配置类
    /// </summary>
    public class TokenParameter
    {
        // 密钥
        public string Secret { get; set; }

        // 签发人（或组织）
        public string Issuer { get; set; }

        // 受众
        public string Audience { get; set; }

        // 有效时间 单位是分钟
        public int AccessExpiration { get; set; }

        // RefreshToken的有效时间单位分钟
        public int RefreshExpiration { get; set; }
    }
}