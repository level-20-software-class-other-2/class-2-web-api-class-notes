using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Infrastructure.Db;
using Soft2ApiDemo.Domain.Repository;
using Soft2ApiDemo.Domain.Entity;
using Soft2ApiDemo.Infrastructure.Repository;
using Soft2ApiDemo.Infrastructure.Filters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Soft2ApiDemo.Infrastructure.Parameter;
using System.Text;

namespace Soft2ApiDemo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var conString = Configuration.GetConnectionString("SqlServer");

            // IoC 依赖倒转
            // DI  依赖注入

            // DI分两大步，第一步叫注册，注册服务接口和具体实现到容器
            // 第二步，其实就是真正在使用的时候，注入服务到构造函数、属性、特性
            services
                .AddDbContext<Soft2ApiDemoDbContext>(options =>
                {
                    options.UseSqlServer(conString);
                });

            // services.AddControllers();

            services.AddScoped(typeof(IRepositoryBase<>),typeof(RepositoryBase<>));

            // 注册验证器（使用何种配置来验证token）
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(option =>
            {
                // 是否要求使用https
                option.RequireHttpsMetadata=false;
                // 是否保存token
                option.SaveToken=true;
                // 使用配置中间件，获得token的配置
                var tokenParameter=Configuration.GetSection("TokenParameter").Get<TokenParameter>();
                // 验证器的核心属性
                option.TokenValidationParameters=new TokenValidationParameters
                {
                    ValidateIssuerSigningKey=true,//要不要验证生成token的密钥
                    IssuerSigningKey=new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                    ValidateIssuer=true, // 要不要验证发行token的人（个人或者组织）
                    ValidIssuer=tokenParameter.Issuer,
                    ValidateAudience=false // 是否验证受众
                };
            });



            // 办法一
            // var provider=services.BuildServiceProvider();

            // var IRep =provider.GetService<IRepositoryBase<AppLogs>>();         

            
            // services.AddControllers(options=>options.Filters.Add(new MyGlobalExceptionFilter(IRep)));

            // 办法二
            services.AddControllers(options=>{
                options.Filters.Add(typeof(MyGlobalExceptionFilter));
                options.Filters.Add(typeof(CustomAuditLogFilter));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            // 将token的验证注册到中间件，认证中间件必须在鉴权之前
            app.UseAuthentication();

            // 鉴权中间件
            app.UseAuthorization();

            app
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }
    }
}
