using System;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Soft2ApiDemo.Domain.Entity;
using Soft2ApiDemo.Domain.Repository;
using Soft2ApiDemo.Infrastructure.Utils;
using Microsoft.Extensions.Configuration;
using Soft2ApiDemo.Infrastructure.Parameter;
using Microsoft.AspNetCore.Authorization;

namespace Soft2ApiDemo.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IRepositoryBase<Users> _usersRes;

        private readonly IConfiguration _configuration;

        public UsersController(IRepositoryBase<Users> usersRes, ILogger<UsersController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _usersRes = usersRes;
            _configuration = configuration;
        }

        [HttpGet]
        public IEnumerable<dynamic> Index()
        {
            // throw new Exception("这是一个错误，延续了一个世纪的错误");
            _usersRes
                .Add(new Users
                {
                    Username = "老胡来也",
                    Password = "没有密码"
                });
            var lst =
                _usersRes
                    .Table
                    .Select(x =>
                        new
                        {
                            x.Id,
                            x.Username,
                            x.Password,
                            Nickname = "柯文龙不要睡觉"
                        })
                    .ToArray();
            var uuu =
                new List<dynamic> {
                    new {
                        Name = "天天向上",
                        Age = "3389",
                        Sex = "男",
                        Children =
                            new List<dynamic> {
                                new { KidName = "大娃", Age = 1 },
                                new { KidName = "二娃", Age = 0.9 }
                            }
                    },
                    new {
                        Nickname = "你的童年我的童年，好像都一样",
                        Height = 1990
                    }
                };
            _logger.LogInformation(uuu.SerializeJson());
            return uuu;
        }

        [HttpPost]
        public Users AddUser(UserCto model)
        {
            var tmp =
                new Users
                {
                    Username = model.Username,
                    Password = model.Password
                };
            _usersRes.Add(tmp);
            return tmp;
        }

        public class UserCto
        {
            public string Username { get; set; }

            public string Password { get; set; }
        }

        [HttpPut]
        [Route("{id}")]
        public Users UpdateUser(Guid id, UserCto model)
        {
            var entity = _usersRes.GetById(id);
            if (entity != null)
            {
                entity.Password = model.Password;
                _usersRes.Update(entity);
            }
            return entity;
        }

        // [HttpDelete]
        [Route("{id}")]
        public dynamic DeleteUser(Guid id)
        {
            _usersRes.Delete(id);
            return new { Code = 1000, Msg = "删除成功", Data = "" };
        }

        [HttpPost]
        [Route("{id}/friends")]
        public dynamic GetFriends(Guid id, UserCto model)
        {
            var lst =
                new
                {
                    Code = 1000,
                    Msg = "为指定用户新增朋友成功",
                    Data = new { Id = id, Model = model }
                };
            return lst;
        }

        [Route("{id}/friends")]
        public IEnumerable<dynamic> GetFriends(Guid id)
        {
            var lst =
                new List<dynamic> {
                    new { Name = "唐大腿", Age = 16 },
                    new { Name = "王文文", Age = 28 }
                };

            return lst;
        }

        [Route("{id}/friends/{friendId}")]
        public dynamic GetFriends(Guid id, Guid friendId)
        {
            var lst =
                new
                {
                    Code = 1000,
                    Msg = "成功",
                    Data = new { Name = "唐大腿", Age = 16 }
                };

            return lst;
        }

        [AllowAnonymous]
        [Route("/auth")]
        public string Login()
        {
            var tokenParameter = _configuration.GetSection("TokenParameter").Get<TokenParameter>();
            var token = TokenHelper.GenerateToken(tokenParameter);

            var res = new
            {
                Code = 1000,
                Msg = "认证成功",
                Data = token
            };
            return res.SerializeJson();
        }

        [AllowAnonymous]
        [Route("/refreshtoken/{token}")]
        public string Login(string token)
        {
            var tokenParameter = _configuration.GetSection("TokenParameter").Get<TokenParameter>();
            var refreshToken=TokenHelper.RefreshToken(tokenParameter,token);
            return refreshToken.SerializeJson();
        }
    }
}
