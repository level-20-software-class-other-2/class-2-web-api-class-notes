using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Soft2ApiDemo.Domain.Entity;
using Soft2ApiDemo.Infrastructure.Utils;

namespace Soft2ApiDemo.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FilesController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public FilesController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        [Route("/files")]
        public async Task<string> OnPostUploadAsync(IFormCollection files)
        {
            // long size = files.Sum(f => f.Length);

            // （通过配置文件获取到的）设置上传文件
            var uploadFilePath = _configuration.GetValue<string>("UploadFilespath");
            // 当前路径（完整路径）
            var currentPath = Directory.GetCurrentDirectory();

            // 用于容纳要返回的相对地址
            var list = new List<string>();

            // 遍历上传的文件
            foreach (var formFile in files.Files)
            {
                if (formFile.Length > 0)
                {
                    // 随机文件名称
                    var rndFileName = Guid.NewGuid().ToString("N");

                    // 扩展名
                    var extenName = formFile.FileName.Substring(formFile.FileName.IndexOf("."));

                    // 新的文件名（包含扩展名）
                    var newFileName = string.Format("{0}{1}", rndFileName, extenName);

                    var now = DateTime.Now;

                    // 相对路径（不包含当前路径、不包含新文件路径）
                    var relativePath = Path.Combine(uploadFilePath, now.Year.ToString(), now.Month.ToString(), now.Day.ToString());

                    // 当前完整路径 + 相对路径
                    var preFileName = Path.Combine(currentPath, relativePath);

                    // 路径不存在则创建
                    if (!Directory.Exists(preFileName))
                    {
                        Directory.CreateDirectory(preFileName);
                    }

                    //  完整文件名称（包含路径、扩展名）
                    var fullPath = Path.Combine(preFileName, newFileName);

                    // 写入硬盘
                    using (var stream = System.IO.File.Create(fullPath))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                    // 返回的文件路径（一般为相对路径）
                    list.Add(Path.Combine(relativePath, newFileName).Replace("\\","/"));
                }
            }



            return list.SerializeJson();
        }
    }
}