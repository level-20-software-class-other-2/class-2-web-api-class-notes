# 2022-06-02 天气：晴转雨 心情：尚可，但是为啥端午不放假呢？？？ 课程：关于CRUD的实现

## 1.Demosoft.Domain中实现一个借口

### IRepository
```js

using System;
using System.Linq;
namespace Demosoft2.Domain.IRepository
{
    /// <summary>
    /// 接口
    /// </summary>
        public interface IRepository<T>
    {
        T GetById(Guid id);
        IQueryable<T> Table { get; }
        void Insert(T entity);
        void Updatae(T entity);
        void Delete(Guid id);
    }
}
```

## 2.Demosoft.Infrastructure中实现接口的内容
```js
using System;
using System.Linq;
using Soft2ApiDemo.Domain;
using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Domain.Repository;
using Soft2ApiDemo.Infrastructure.Db;
namespace Demosoft.Infrastructure.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly Demosoft2Context _db;
        private DbSet<T> _table;
        public RepositoryBase(Demosoft2Context db)
        {
            _db = db;
            _table = _db.Set<T>();
        }

        public T GetById(Guid id)
        {
            var entity = _table.Where(x => x.Id == id).FirstOrDefault();
            return entity;
        }

        public IQueryable<T> Table
        {
            get
            {
                if (_table == null)
                {
                    _table = _db.Set<T>();
                }
                return _table.AsNoTracking().AsQueryable<T>();
            }
        }

        public void Insert(T entity)
        {
            _db.Add(entity);
            _db.SaveChanges();
        }
        public void Update(T entity)
        {
            _db.Update(entity);
            _db.SaveChanges();
        }
        public void Delete(Guid id)
        {
            _db.Remove(id);
            _db.SaveChanges();
        }
    }
}
```