## 2022-06-05 天气：晴转大雨  心情：尚可  课程：关于CRUD（二）

## 依赖注入

1.方法
```
到配置服务类（Startup）的 public void ConfigureServices(IServiceCollection services){  } 方法中注册，注册方式有 addSingleton() addTransient() addScoped() 三种

例：services.AddSingleton<接口名, 实现类 类名>();
```

2.代码展示
### Startup.cs
```js
public void ConfigureServices(IServiceCollection services)
        {
            var conString = Configuration.GetConnectionString("SqlServer");//引用的Sql也可以换成Postgresql

            // IoC 依赖倒转
            // DI  依赖注入

            // DI分两大步，第一步叫注册，注册服务接口和具体实现到容器
            // 第二步，其实就是真正在使用的时候，注入服务到构造函数、属性、特性
            services
                .AddDbContext<Soft2ApiDemoDbContext>(options =>
                {
                    options.UseSqlServer(conString);
                });

            services.AddScoped(typeof(IRepositoryBase<>),typeof(RepositoryBase<>));

            
            services.AddControllers();
        }
```

### appSettings.json
```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "SqlServer":"server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;",
    "PostgreSQL":"server=host.9ihub.com;database=soft2_api_demo;uid=postgres;pwd=qq_112358;"
  }//可以在这里添加要引入的内容
}
```

## 第一个作业：指定展示的字段

```js
 var list = _usersRes.Table.Select(x=> new { x.UserName, x.Password, x.Id }).ToArray();

            return list;
```