# 2022-06-19 天气：晴朗 心情：又是想回家的一天 课程：关于token

## 关于token的生成和刷新

1.写入了一个关于tokenHelper的文件为了更方便的引用
```c#
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Threading.Tasks;
using Soft2ApiDemo.Infrastructure.Parameter;
using Soft2ApiDemo.Infrastructure.Dto;

namespace Soft2ApiDemo.Infrastructure.Utils
{
    public static class TokenHelper
    {
        public static string GenerateToken(TokenParameter tokenParameter)
        {
            var claims = new[]
             {
                new Claim("Name", "admin"),
                new Claim("Role", "admin"),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var jwtToken = new JwtSecurityToken(tokenParameter.Issuer, null, claims, expires: DateTime.UtcNow.AddMinutes(tokenParameter.AccessExpiration), signingCredentials: credentials);

            var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            return token;
        }

        public static RefreshTokenDto RefreshToken(TokenParameter tokenParameter, string oldToken)
        {
            // 这里是刷新token的用法，没有数据验证、没有用户标识，也没有经过完美的测试，请勿直接用于生产环境，否则，后果自负

            //这儿是验证Token的代码
            var handler = new JwtSecurityTokenHandler();
            ClaimsPrincipal claim = handler.ValidateToken(oldToken, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false,
            }, out SecurityToken securityToken);
            var username = claim.Identity.Name;
            //这儿是生成Token的代码
            var token = GenerateToken(tokenParameter);
            //此处的RefreshToken是直接指定和字符串，在正式项目中，应该是一个可验证的加密串，如md5、hash等
            var refreshToken = "654321";

            var res = new RefreshTokenDto
            {
                AccessToken = token,
                RefreshToken = refreshToken
            };
            return res;
        }
    }
}
```
2.需要相关的属性，写入一个Parameter的文件夹然后写一个TokenParameter的文件
```C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft2ApiDemo.Infrastructure.Parameter
{
    /// <summary>
    /// Token的配置相对的配置类
    /// </summary>
    public class TokenParameter
    {
        // 密钥
        public string Secret { get; set; }

        // 签发人（或组织）
        public string Issuer { get; set; }

        // 受众
        public string Audience { get; set; }

        // 有效时间 单位是分钟
        public int AccessExpiration { get; set; }

        // RefreshToken的有效时间单位分钟
        public int RefreshExpiration { get; set; }
    }
}
```

3.写入一个Dto(数据传输对象)的文件夹，写入一个RefershTokenDto的文件夹用来刷新token
```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Soft2ApiDemo.Infrastructure.Dto
{
    public class RefreshTokenDto
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
```
4.在startup.cs内注册
```c#
// 注册验证器（使用何种配置来验证token）
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(option =>
            {
                // 是否要求使用https
                option.RequireHttpsMetadata=false;
                // 是否保存token
                option.SaveToken=true;
                // 使用配置中间件，获得token的配置
                var tokenParameter=Configuration.GetSection("TokenParameter").Get<TokenParameter>();
                // 验证器的核心属性
                option.TokenValidationParameters=new TokenValidationParameters
                {
                    ValidateIssuerSigningKey=true,//要不要验证生成token的密钥
                    IssuerSigningKey=new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                    ValidateIssuer=true, // 要不要验证发行token的人（个人或者组织）
                    ValidIssuer=tokenParameter.Issuer,
                    ValidateAudience=false // 是否验证受众
                };
            });

            
 public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            // 将token的验证注册到中间件，认证中间件必须在鉴权之前
            app.UseAuthentication();

            // 鉴权中间件
            app.UseAuthorization();

            app
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }
```
## 