# 2022-06-13 天气：早上下了一场暴雨，直接把操场淹了 心情：困 课程：关于过滤去捕获全局异常

### 捕获全局异常

### Filter下的MyGlobalException.cs
```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
namespace Soft2ApiDemo.Infrastructure.Filters
{
    public class MyGlobalExceptionFilter : IAsyncExceptionFilter
    {
       
        public Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.ExceptionHandled == false)
            {
                string msg = context.Exception.Message;
                context.Result =
                    new ContentResult {
                        Content = msg,
                        StatusCode = 200,
                        ContentType = "text/html;charset=utf-8"
                    };
            }
            context.ExceptionHandled = true; //异常已处理了

            return Task.CompletedTask;
        }
    }
}
```
### 在主页面下写入一个异常
```C#
 throw new Exception("这是一个错误，延续了一个世纪的错误");
```