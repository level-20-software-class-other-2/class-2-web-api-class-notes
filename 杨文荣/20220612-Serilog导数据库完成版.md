~~~c#
下载using Serilog.Sinks.MSSqlServer;
最小api配置
builder.Services.AddLogging(build=>{
     Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .CreateLogger();
            build.AddSerilog();
}
);
builder.Host.UseSerilog((context, services, configuration) =>
                    configuration
                        .ReadFrom
                        .Configuration(context.Configuration)
                        .ReadFrom
                        .Services(services)
                        .Enrich
                        .FromLogContext()
                        .WriteTo
                        .Console()
                        .WriteTo.MSSqlServer(ConnectionStrings,sinkOptions: new MSSqlServerSinkOptions{TableName="logs",AutoCreateSqlTable=true}));


或者用new LoggerConfiguration()
    .WirteTo.MSSqlServer(connetcionString,
        sinkOptions: new MSSqlServerSinkOptions { TableName = "LogEvents", needAutoCreateTable = true})
    .CreateLogger();

~~~
