# 一、简单复习一下c#中的 static：
#
```c#
1、静态类：如果一个类，被声明为静态类，那么该类不可以被实例化，不可以包含非静态成员，同时也不可以被继承，
public static class JsonHelper
{
    ...
}


2、静态方法：在方法前用static修饰，表示此方法为所在类所有，而不是这个类的实例所有
public static string toJsonObject(this object obj)
{
    ...
}
将来调用只需要: 静态类.静态方法名称  而不能实例化！！!  总结说静态方法就是为了可以被简单化的多次调用



3、静态函数：
static int a=1;表示每次重新使用该变量所在方法、类或自定义类时，变量的值为程序这次运行最后一次为变量赋值时的值
```
#
#
# 二、巧用静态方法调用System.Text.Json序列化自定义对象
#
## 1、想法：
```c#
上面说到静态方法直接 静态类.静态方法即可，而搭配控制器中using引用，直接可以.静态方法使用，所以在静态类中来写System.Text.Json序列化方法
```
#
## 2、静态类代码:
```c#
using System.Text.Json;
using System.Text.Encodings.Web;

namespace webapi_infrastructure.JsonHelper
{
    public static class JsonHelper
    {
        public static string toJsonObject(this object obj)//this object 代表调用者本身
        {
            JsonSerializerOptions  options = 
                new JsonSerializerOptions {
                    // Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,未涉及字符转义可不写
                    WriteIndented = true
                };
                return JsonSerializer.Serialize(obj, options);
        }
    }
}
```
#
## 3、简单解析所需类的原因：
```c#
        1、JsonSerializer：
                1 继承于public static class JsonSerializer 类

                2 命名空间: System.Text.Json

                3 作用: 提供将对象或值类型序列化为 JSON 以及将 JSON 反序列化为对象或值类型。  
                上面的JsonSerializer 继承于 public static class JsonSerializer(因为static且using了所以直接用) 

                4 类中方法(非常多!只看需要的)
                   1. Serialize(object?value, Type, options)：

                        作用：将指定类型的值转换为 JSON 字符串

                        参数：
                              object?value：要转换的值  
                              Type：要转换的 value 的类型
                              options：JsonSerializerOptions 用于控制转换成啥的行为选项

                5 参考：
                https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonserializer?view=net-6.0 
            





        2、JsonSerializerOptions 
                1 继承于public sealed(密封类) class JsonSerializerOptions(同) 类
                
                2 命名空间: System.Text.Json
                
                3 作用：用于控制转换应该用哪些具体行为的选项

                3 构造函数：JsonSerializerOptions()等、有三个
                
                4 属性(只看需要用的)：
                    1.Encoder：
                        作用：“获取或设置要在转义字符串时”(但是这里不需要转义,所以可以不写!)使用的编码器，或为 null（要使用默认编码器的话）
                        
                        定义：public System.Text.Encodings.Web.JavaScriptEncoder? Encoder { get; set; }

                        值：引出使用 JavaScriptEncoder 类原因：
                                1 继承于public abstract class JavaScriptEncoder : System.Text.Encodings.Web.TextEncoder 类
                                
                                2 命名空间:System.Text.Encodings.Web 
                                    表示 JavaScript 字符编码
                                
                                3 构造函数：JavaScriptEncoder() 初始化 JavaScriptEncoder 类的新实例   
                                
                                4 属性(只看需要用的)：
                                    1 UnsafeRelaxedJsonEscaping：
                                        作用：获取对编码内容不太严格(Default值不会转义HTML敏感字符,例如<， >&,需谨慎使用)的内置 JavaScript 编码器实例

                                        定义：public static System.Text.Encodings.Web.JavaScriptEncoder UnsafeRelaxedJsonEscaping { get; }
                                5 参考：
                                https://docs.microsoft.com/zh-cn/dotnet/api/system.text.encodings.web.javascriptencoder?view=net-6.0






                    2.WriteIndented：
                        作用：获取或设置一个值，该值指示 JSON 是否应使用漂亮的打印。 默认情况下，不使用任何额外的空白来序列化 JSON
                        
                        定义：public bool WriteIndented { get; set; }
                    
                5 参考：
                https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonserializeroptions?view=net-6.0
```
#
## 效果如图： 
# ![a](./images/0.png)
#
#
# 三、以上参考均来源于.NET微软文档： 
https://docs.microsoft.com/zh-cn/dotnet/fundamentals/
#
#
# 四、过了一个晚上，觉得就目前为数不多的返回数据应该直接手动写序列化方法
```c#
//引入手动序列化所需命名空间
using System.Text.Json;

{
    _logger.LogInformation(JsonSerializer.Serialize(res,new JsonSerializerOptions{WriteIndented=true}));
    return res;
}

效果一样一样的，在迷你项目中很实用
```