#
# 一、已改进详情见2022-6-05 新版 不需要对Startup.cs文件作任何改动
#
#
# 二、搭配序列化使用
```c#
WeatherForecast控制器中：

    public IEnumerable<WeatherForecast> Get()
    {
        //开始日志
        _logger.LogInformation("成功进去Get方法,开始录入具体信息...");
        
        xxx....

        //序列化
        _logger.LogInformation(res.toJsonObject());
            
        return res;
    }
```
