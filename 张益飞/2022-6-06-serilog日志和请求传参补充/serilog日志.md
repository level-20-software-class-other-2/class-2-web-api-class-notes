# Serilog（日志）----绝对不能让没有任何监控的项目上线的！！！ 
#
# 一、作用：
```
记录外部对项目某个控制器发起的请求和该控制器对你做出的响应
```
#
#
# 二、包

```js
将Serilog.AspNetCore NuGet软件包安装到您的应用程序中(注意是添加到基础设置项目webapi_infrastructure中!)

    dotnet add package Serilog.AspNetCore
```
#
#
# 三、实现：这里有两种办法
#
## 1.法一： 直接new LoggerConfiguration实例去重写Microsoft自带的日志方法
```c#

这种方法不需要appsettings.json和appsettings.Development.json文件，仅仅只要在Program.cs添加代码


 1 Main方法中：
    public static void Main(string[] args)
    {
    	// 配置 Serilog 
        Log.Logger = new LoggerConfiguration()
            // 最小的日志输出级别
            .MinimumLevel.Information()
            // 日志调用类命名空间如果以 Microsoft 开头，覆盖日志输出最小级别为 Information，像是重写方法
            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            .Enrich.FromLogContext()
            // 配置日志输出到控制台
            .WriteTo.Console()
            // 配置日志输出到文件，文件输出到LogFiles\log.txt中并且日记的生成周期为每天
            .WriteTo.File(Path.Combine(@"..\LogFiles\log.txt"), rollingInterval: RollingInterval.Day)
            // 创建 logger,这个写完不能再写别的了,因为已经创建成功了,再写的不生效!!!
            .CreateLogger();

            try{
                Log.Information("开始启动主机日志并写入文件...");//Starting web host
                CreateHostBuilder(args).Build().Run();
            }
            catch{
                Log.Fatal(ex, "主机意外终止");//Host terminated unexpectedly
            }
    }


 2 CreateHostBuilder方法中：
    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .UseSerilog() // <-添加此行--调用Serilog,覆盖掉CreateDefaultBuilder中的默认配置(也就是下面第二种方法,所以这种方法不能从appsettings.json拿数据,只能new LoggerConfiguration并同时写好配置) 
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });

```
#
#
## 1.法二： main方法中只加try...catch...，只在CreateHostBuilder方法用源码写好的Configuration调用logger配置
```c#

这种方法需要往appsettings.json或appsettings.Development.json文件添加Serilog配置信息(谁添加都可以,有一个存在即可,所以不能判断这两个文件谁被先调用,没试出结论唉.)  
 

 1 appsettings.json或appsettings.Development.json添加
   "Serilog": {
        "MinimumLevel": {
            "Default": "Information",
            "Override": {
                "Microsoft": "Warning",
                "Microsoft.Hosting.Lifetime": "Information"
            }
        },
        "WriteTo": [{
            "Name": "File",
            "Args": { "path": "../LogFiles/log-.txt", "rollingInterval": "Day" }
        }]
    }


 2 CreateHostBuilder方法中：
    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .UseSerilog((context, services, configuration) => configuration
            .ReadFrom.Configuration(context.Configuration)
            //.ReadFrom.Services(services)
            //.Enrich.FromLogContext() 目前这两个并无啥作用 只知道一个在给数据库同步日志用到的 Enricher 的作用主要是增加记录的信息，比如Enrich.WithThreadId()，可以记录线程信息，Enrich.WithProperty()可以增加属性信息--可以参数这篇文章：https://www.cnblogs.com/weihanli/p/custom-serilog-enricher-to-record-more-info.html
            .WriteTo.Console())
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });

```
#
#
# 四、 最后不管哪种方法，调试启动vs，都可以看到以下效果
# ![a](./images/0.png)
#
#
# 五、在项目中使用 Serilog 为控制器进行日志输出
```c#
WeatherForecast控制器中：
  
  public class WeatherForecastController : ControllerBase
  {
    //创建字段  
    private readonly ILogger<WeatherForecastController> _logger;

    //依赖注入
    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    public IEnumerable<WeatherForecast> Get()
    {
        //真正使用
        _logger.LogInformation("成功进去Get方法,开始录入具体信息...");
        ...
          响应体...
        ...
    }
  }
```
#
#
# 六、效果
## 方法被执行时日志文件内容如图:
#
# ![a](./images/2.png)
#
#
# 七、参考
```
1.第一种方法：
https://zhuanlan.zhihu.com/p/263815555

2.两种方法都有：
https://blog.csdn.net/Upgrader/article/details/88323907
```