# "惊心动魄"的日子已经过去了....
#
# 一、起步
```js
 1 创建项目
    dotnet new webapi -o Demo.Api --no-https

 2 创建解决方案
    dotnet new sln -n Demo

 3 解决方案添加项目
    cd ..
    dir dotnet sln add Demo.api (dir中必须要有sln文件!)
```
#
#
# 二、using程序集基础
```c#
1 随机数(Random)和时间函数(DateTime)
  using System;

2 泛型集合 IEnumerable<T>
  using System.Collections.Generic 

3 引用 Mvc 的 Controller和 HttpGet 接口
  using Microsoft.AspNetCore.Mvc;
```
#
#
# 三、通配路由规则
```c# 
一、常规路由规则：(数组型)
1 要求控制器中有且仅有唯一的方法(方法名称不限制)
  [Route("[controller]")] 
组合:
2 控制器多个方法的通配路由规则              
  [Route("[controller]/[action]")]   

二、非常规路由规则：(字符串型,注意不是字符!)
1 [Route("xxx")] 
组合:
2 [Route("xxx/[action]")]
```
#
#
# 四、方法返回值
```js
1 数组
string[]

2 伪数组IEnumerable<T>
IEnumerable<string>
```
#
#
# 五、请求参数初步
```c#
    [HttpGet]
    public int Getid(int id) // Text/Getid?id=3
    {
        return id+1;
    }
    //无论?后面是否是id都会进入,int类型默认为0
```
#
# https://www.cnblogs.com/lys1599536/p/15760738.html