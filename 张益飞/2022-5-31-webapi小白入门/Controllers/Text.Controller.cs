using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
// using System.Web.Http.Filters;

namespace Dome_Api.Controllers
{
    //[ApiController]
    [Route("Text/[action]")]
    public class TextController : ControllerBase
    {
        [HttpGet]
        public string Main()
        {
                    
            return "holle world";
        }

        [HttpGet]
        public int Getnumber()
        {
            var rng = new Random();
            return  rng.Next(-20, 55);
        }

        //字符串数组
        [HttpGet]
        public string[] Getarr() //IEnumerable<string>
        {
            return new string[]{"a","b","c"};
        }

        //參數
        [HttpGet]
        public int Getid(int d) // Text/Getid?id=3
        {
            return d;
        }
    }
}