# 一、(IA)syncActionFilter源于AOP概念
```
AOP：在不修改原有代码的基础上，动态增加业务逻辑或修补bug
```
## 图解：
# ...... ![a](./images/0.png)
# ![a](./images/1.png)
#
# 
# 二、.net core 中AOP的六大--Filter
# ![a](./images/2.png)
#
#
# 三、这里只讲(IA)syncActionFilter
```c#
1、因为极度逼近Action，所以非常适合用于日志操作

2、快速示例-异步IAsyncActionFilter

        1 AuditActionFilter 类中代码:
                public class AuditActionFilter: IAsyncActionFilter
                {
                    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next) 
                    {
                        //在执行Action方法前做点什么... 

                        ActionExecutedContext result =  await next();//方法执行中...

                        //在执行Action方法后做点什么... 
                    }
                }
            
            /*
                参数：
                1、ActionExecutingContext context
                    原类型的context包含：
                        1 获取调用操作时传递的参数：ActionArguments
                          var arguments = context.ActionArguments;

                        参考：https://docs.microsoft.com/zh-cn/dotnet/api/microsoft.aspnetcore.mvc.filters.actionexecutingcontext?view=aspnetcore-6.0



                    被as转化为ControllerActionDescriptor类型的context包含：    
                        1 接口类型信息：ControllerTypeInfo
                          var type = (context.ActionDescriptor as ControllerActionDescriptor).ControllerTypeInfo.AsType();

                        2 发现方法的属性并提供对方法元数据的访问：MethodInfo
                          var method = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;
 
                         参考：https://docs.microsoft.com/zh-cn/dotnet/api/microsoft.aspnetcore.mvc.controllers.controlleractiondescriptor?view=aspnetcore-6.0    


                2、ActionExecutionDelegate next
                    因为相比于同步少了ActionExecutedContext类型的 context参数 所以next是一个异步返回ActionExecutedContext的委托，用于指示操作(上面的示例代码有执行流程)

            */
        
        2 只要在Startup.cs中将AuditActionFilter类注入进Filters即可全局对控制器的所有方法进行监视:
                public void ConfigureServices(IServiceCollection services)
                {
                    ....

                    //注入过滤器
                    services.AddControllers(option => {
                        option.Filters.Add(typeof(AuditActionFilter));
                    });

                    ....
                }
```
## 图解：
# ![a](./images/3.png)