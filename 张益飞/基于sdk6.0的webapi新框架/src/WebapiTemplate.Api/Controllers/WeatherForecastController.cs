using Microsoft.AspNetCore.Mvc;
using WebapiTemplate.Infrastructure.Persistence;
using WebapiTemplate.Domain.Entity;

namespace WebapiTemplate.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;
    //上传文件的信息表
    // private readonly EfRepository<FilesInfo> _filesInfo;
    public EfRepository<FilesInfo> _filesInfo = new EfRepository<FilesInfo>();
    

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    public IEnumerable<WeatherForecast> Get()
    {
        return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        {
            Date = DateTime.Now.AddDays(index),
            TemperatureC = Random.Shared.Next(-20, 55),
            Summary = Summaries[Random.Shared.Next(Summaries.Length)]
        })
        .ToArray();
    }

    //测试图片文件上传
    [HttpPost]
    [Route("Upload/FormImg")]
    public dynamic Getimages(List<IFormFile> files)
    {
        //消除跨域问题
        HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");

        if (files.Count < 1)
        {
            return "文件为空";
        }
        //返回保存文件的详细信息
        List<dynamic> saveFilesInfo = new List<dynamic>();

        FilesInfo objinfo = new FilesInfo();
        //手动一个存放图片的文件路径名称
        var now = DateTime.Now;
        // var filePath = string.Format(
        //     "/Uploads/{0}/{1}/{2}/",
        //     now.ToString("yyyy"),
        //     now.ToString("yyyyMM"),
        //     now.ToString("yyyyMMdd")
        // );

        //获取当前web目录
        var webapiRootPath = Directory.GetCurrentDirectory();
        //存放文件的目录
        var fileRootPath = webapiRootPath + "/Uploads";
        //如果目录不存在就创建
        if (!Directory.Exists(fileRootPath))
        {
            Directory.CreateDirectory(fileRootPath);
        }
        //开始写入文件...
        try
        {
            foreach (var item in files)
            {

                if (item != null)
                {

                    #region  图片文件的条件判断
                    /*
                    //文件后缀
                    var fileExtension = Path.GetExtension(item.FileName);

                    //判断后缀是否是图片
                    const string fileFilt = ".gif|.jpg|.jpeg|.png";
                    if (fileExtension == null)
                    {
                        break;
                        //return Error("上传的文件没有后缀");
                    }
                    if (
                        fileFilt.IndexOf(fileExtension.ToLower(), StringComparison.Ordinal)
                        <= -1
                    )
                    {
                        break;
                        //return Error("请上传jpg、png、gif格式的图片");
                    }

                    //判断文件大小
                    long length = item.Length;
                    if (length > 1024 * 1024 * 2) //2M
                    {
                        break;
                        //return Error("上传的文件不能大于2M");
                    }
                    */
                    #endregion
                    //原文件名
                    var OriginalName = item.FileName;
                    //原文件后缀
                    var Suffix = Path.GetExtension(OriginalName);
                    //文件类型
                    var FileType = item.ContentType;
                    //上传日期获取时间戳
                    var SaveDateTime = DateTime.Now.ToString("yyMMddhhmmssfff");


                    //最后的保存文件的完整路径
                    // var SaveFileName = webapiRootPath + strDateTime;
                    var SaveFileName = Path.Combine(fileRootPath,SaveDateTime)+Suffix;
                    //插入图片数据
                    using (
                        //在指定路径中创建文件
                        FileStream fs = System.IO.File.Create(SaveFileName)
                    )
                    {
                        //从当前item流中读取字节并将它们写入另一个fs文件流。两个流的位置都按复制的字节数前进
                        item.CopyTo(fs);
                        //清除此fs流的缓冲区，使得所有缓冲数据都写入到文件中
                        fs.Flush();
                    }
                        objinfo.OriginalName = OriginalName;
                        objinfo.Suffix = Suffix;
                        objinfo.FileType = FileType;
                        objinfo.SaveDateTime = SaveDateTime;
                        objinfo.SaveFileName =SaveFileName;
                    };
                    saveFilesInfo.Add(objinfo);
                }
            
    

            //上传进数据库表
            _filesInfo.Add(objinfo);

            //上传成功返回后端如何处理的详细信息
            return new
            {
                code = 1000,
                saveFilesInfo = saveFilesInfo
            };

        }
        catch (Exception ex)
        {
            //上传失败
            return new
            {
                code = 400,
                msg = ex.ToString()
            };
        }
    }
}
//https://blog.51cto.com/u_11990719/4025765
//https://blog.csdn.net/qq_41841878/article/details/89996131

/*
1 原文件名称
2 现文件名称
3 后缀
4 格式

6 配置文件
异步形式

7 测试几个参数类型
8 接口-实现类
9 数据库
10 过滤图片
*/