namespace WebapiTemplate.Domain.Entity
{
    public class User : Base.BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
