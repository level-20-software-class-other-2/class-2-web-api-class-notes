// using System;
namespace WebapiTemplate.Domain.Entity
{
    /// <summary>
    /// 用户角色实体类型
    /// </summary>
    public class UserRole : Base.BaseEntity
    {
        /// <summary>
        ///用户Id
        /// </summary>
        /// <value></value>
        public Guid UserId { get; set; } //Guid

        /// <summary>
        ///角色Id
        /// </summary>
        /// <value></value>
        public Guid RoleId { get; set; } //Guid

        /// <summary>
        /// 用户实例
        /// </summary>
        /// <value></value>
        public User User { get; set; }

        /// <summary>
        ///角色实例
        /// </summary>
        /// <value></value>
        public Role Role { get; set; }
    }
}