// using System;

namespace WebapiTemplate.Domain.Entity
{
    public class FilesInfo : Base.BaseEntity
    {
        //文件原名称
        public string OriginalName { get; set; }
        //文件后缀
        public string Suffix { get; set; }
        //文件类型
        public string FileType { get; set; }
        //文件上传时间
        public string SaveDateTime { get; set; }
        //保存文件的完整路径
        public string SaveFileName { get; set; }
    }
}