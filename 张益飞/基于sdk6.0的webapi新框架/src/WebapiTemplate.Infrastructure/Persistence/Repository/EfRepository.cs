using WebapiTemplate.Application.Common;
using WebapiTemplate.Domain.Base;
using System;
using Microsoft.EntityFrameworkCore;

namespace WebapiTemplate.Infrastructure.Persistence
{
    public class EfRepository<T> : IRepository<T> where T:BaseEntity
    {
        private readonly WebapiDbcontext _db;
        public DbSet<T> _table;
        

        public EfRepository()//添加数据库的构造函数且参数不能为null，只能这样了
        {
            _db = new WebapiDbcontext();
            _table = _db.Set<T>();
        }
    
        //新增
        public void Add(T entity)
        {
            entity.IsActived = true;
            entity.IsDeleted = false;
            //   entity.CreatedBy = 0;
            //   entity.UpdatedBy = 0;
            entity.UpdatedAt = DateTime.Now;
            entity.DisplayOrder = 0;
            entity.Remarks = "没有评论";

            _table.Add(entity);
            _db.SaveChanges();
        }
    }
}