using Microsoft.EntityFrameworkCore;
using WebapiTemplate.Domain.Entity;


namespace WebapiTemplate.Infrastructure.Persistence
{
    public class WebapiDbcontext : DbContext
    {
        //为注入数据库依赖提供的构造函数
        // public WebapiDbcontext(DbContextOptions<WebapiDbcontext> options): base(options)
        // {

        // }

        private readonly string constr = "server=.;database=webapidatabase4;uid=sa;pwd=123456;";

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(constr);
        }
        public DbSet<FilesInfo> FilesInfo {get;set;}
    }
    
}