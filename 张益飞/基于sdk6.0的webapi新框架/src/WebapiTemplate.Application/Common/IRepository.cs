namespace WebapiTemplate.Application.Common
{
    public interface IRepository<T>
    {
        //查寻整表
        // IQueryable<T> Table{get;set;}

        //新增
        void Add(T entity);
    }
}