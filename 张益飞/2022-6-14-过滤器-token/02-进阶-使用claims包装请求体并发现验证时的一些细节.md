# 第一、生成token  
```c#

    话不多说，直接上Token静态类来生成安全性更高请求信息更全的token
    1 Token静态类中：

        using System.Text;//Encoding
        using System;//生成令牌的构造函数expires参数需要DateTime
        using Microsoft.IdentityModel.Tokens;//SymmetricSecurityKey、SigningCredentials、SecurityAlgorithms
        using System.IdentityModel.Tokens.Jwt;//JwtSecurityToken、JwtSecurityTokenHandler

        using System.Security.Claims;//<Claim>
        using System.Collections.Generic;//List<Claim>

        namespace webapi_infrastructure.CreateToken
        {
            public class Token
            {
                public static string GetToken()
                {
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("111111111111111111"));
                    var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    //JwtSecurityToken对象六个参数我直接随便写的，所以切记等下判断的时候也要一样，下节再利用appsettings.json文件封装这些参数
                    var token = new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(//我就直接一步类型转化并字符串紧凑化了
                        "Issuer",
                        "Audience",
                        new List<Claim>
                        {
                            new Claim(ClaimTypes.Role, "admin"),
                            new Claim(ClaimTypes.Gender,"Male"),
                            new Claim(ClaimTypes.Name,"zyf"),
                        },
                        expires: DateTime.UtcNow.AddMinutes(30),
                        signingCredentials: credentials
                    ));

                    //将生成的token返回给请求者
                    return token;
                }
            }
        }
```
#
## 效果如图:
# ![a](./images/0.png)
#
#
#
# 第二、Startup.cs 验证token
```c#
    1 在Startup.cs中注册捕获token和判断token逻辑
        //引入拦截请求的token所需的命名空间
        using Microsoft.AspNetCore.Authentication.JwtBearer;
        using System.Text;
        using Microsoft.IdentityModel.Tokens;


        ....
        public void ConfigureServices(IServiceCollection services)
        {
            ....
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        //因为我在生成token时多加了Issuer、Audience两个参数的值，所以需要判断

                        /*
                            但是这里有个bug(算这个包自带bug吧)，就是如果你不想判断这两个参数，那就必须写明白ValidateAudience = false,不然默认为true! 那这个时候又没写验证参数ValidAudience = "Audience",所以结果就是验证失败，等下下面有图A!
                        */
                        ValidateAudience = true,//false
                        ValidAudience = "Audience",

                        ValidateIssuer = true,//false
                        ValidIssuer = "Issuer",

                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("111111111111111111")),

                        //这个是验证token中令牌的生命周期(就是有效期)
                        ValidateLifetime = true,
                        //有效期为2分钟，超过就授权失败
                        ClockSkew = TimeSpan.FromMinutes(1),
                    };
                });
        }
```
#
## 效果如图:
# ![a](./images/1.png)
#
#
#
# 2.1、补上验证时提到的图A设置false验证失败情况
# ![a](./images/2.png)
# 
#
#
# 三、验证生命周期超时--当我的Startup.cs验证是生命周期参数设置1分钟：ClockSkew = TimeSpan.FromMinutes(1)
# 效果如图：
# ![a](./images/4.png)
#
#
#
#
# 四、这里补充一个当JwtSecurityToken对象六个参数的Issuer、Audience两个值为null时可能出现的bug
```c#
    Token静态类中：
        ....
        省略的代码同上面第一个有!
        ....
            var token = new JwtSecurityTokenHandler().WriteToken( new JwtSecurityToken(
                        null,// Issuer 值为null
                        null,// Audience 值为null
                        new List<Claim>
                        {
                            new Claim(ClaimTypes.Role, "admin"),
                            new Claim(ClaimTypes.Gender,"Male"),
                            new Claim(ClaimTypes.Name,"zyf"),
                        },
                        expires: DateTime.UtcNow.AddMinutes(1),
                        signingCredentials: credentials
                    ));

```
#
## 1、第一个错误的方式：在Startup.cs类中我的验证可能是按 true值为null 方式--产生bug了^0^
```c#
        ....
        省略的代码同上面第一个有!
        ....
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ....
                ....
                    ValidateAudience = true,
                    ValidAudience = null,

                    ValidateIssuer = true,
                    ValidIssuer = null,
                ....
                ....
            };
```
#
## 结果如图：(这个意思是验证值是错误的，就相当于你密码输错了一样)
# ![a](./images/3.png)
```
原因是：当我生成token时这两个参数值为null可以是可以，但验证时就必须不进行验证，因为能进行验证的值前提是不能为空！
```
#
#
#
#
## 2、第二：正确的验证方法
```c#
        ....
        省略的代码同上面第一个有!
        ....
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ....
                ....
                    ValidateAudience = false,

                    ValidateIssuer = false,
                ....
                ....
            };
```
#
## 这样结果又是正确的了0.0
# ![a](./images/1.png)