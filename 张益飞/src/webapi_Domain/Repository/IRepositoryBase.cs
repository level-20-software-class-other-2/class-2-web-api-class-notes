using System;
using System.Linq;
using System.Threading.Tasks;

namespace webapi_Domain.Repository
{
    public interface IRepositoryBase<T>
    {
        //属性
        IQueryable<T> Table{get;set;}//可以不用set

    //方法
        //查询
        T GetById(Guid id);//Guid
        //新增
        void Add(T entity);
        //编辑
        void Update(T entity);
        //删除
        void Deleted(Guid id);//Guid

        //审计日志表的新增功能
        Task SaveAsync(T entity);
    }
}