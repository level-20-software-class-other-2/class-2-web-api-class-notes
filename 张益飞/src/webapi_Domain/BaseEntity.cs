using System;
namespace webapi_Domain
{
    public abstract class BaseEntity
    {
        //主键
        public Guid Id {get;set;}//public Guid Id {get;set;}
        
        //标识是否启用
        public bool IsActived {get;set;}

        //是否删除
        public bool IsDeleted {get;set;}

        //创建者
        public Guid CreatedBy {get;set;}

        //最后的修改这
        public Guid UpdatedBy {get;set;}

        //更新时间
        public DateTime UpdatedAt {get;set;}

        //展示顺序
        public int DisplayOrder {get;set;}

        //备注
        public string Remarks {get;set;}
    }
}