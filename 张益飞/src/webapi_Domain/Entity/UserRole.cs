using System;
using webapi_Domain;
namespace webapi_Domain.Entity
{
    /// <summary>
    /// 用户角色实体类型
    /// </summary>
    public class UserRoles : BaseEntity
    {
        /// <summary>
        ///用户Id
        /// </summary>
        /// <value></value>
        public Guid UserId { get; set; } //Guid

        /// <summary>
        ///角色Id
        /// </summary>
        /// <value></value>
        public Guid RoleId { get; set; } //Guid

        /// <summary>
        /// 用户实例
        /// </summary>
        /// <value></value>
        public Users User { get; set; }

        /// <summary>
        ///角色实例
        /// </summary>
        /// <value></value>
        public Roles Role { get; set; }
    }
}