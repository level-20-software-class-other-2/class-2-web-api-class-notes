using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using webapi_Domain.Entity;
using webapi_Domain.Repository;
using System;


namespace webapi.Filter
{
    public class ExceptionFilter : IAsyncExceptionFilter
    {

        //异常记录表字段
        private readonly IRepositoryBase<ExceptionTable> _exceptionTable;
                
        //通过依赖注入拿到真正的表
        public ExceptionFilter(IRepositoryBase<ExceptionTable> exceptionTable)
        {
            _exceptionTable = exceptionTable;
        }

            

        public  Task OnExceptionAsync(ExceptionContext context)//context-获取或设置已处理的指示 Exception 
        {
            var tmpuser =
                new ExceptionTable
                    {
                         Exception_msg = context.Exception.Message//context.Exception.ToString()
                    };
                    
            _exceptionTable.Add(tmpuser);

            // ExceptionHandled-获取或设置一个值，该值指示是否已处理异常
            if(context.ExceptionHandled==false)
            {
                //自定义异常的返回对象
                var res = new {
                    code=1000,
                    msg="发现异常，错误信息如下：",
                    //异常的详细信息
                    error = context.Exception.Message 
                };
                
                //context.Result 是抛出异常的返回值，类型是Microsoft.AspNetCore.Mvc.IActionResult，而ContentResult类在执行 ActionResult 时将生成包含内容的响应，且返回值恰好是IActionResult类型
                context.Result = new ContentResult
                {
                    Content = res.ToString()
                };
            }
            
           
            //告诉系统异常已经被人为处理了,不需要系统再抛出自带的异常
            context.ExceptionHandled = true;
        
            //Task.CompletedTask本质上来说是返回一个已经完成的Task对象
            return  Task.CompletedTask;

        }
    }
}