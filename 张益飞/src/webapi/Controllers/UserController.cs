using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webapi_Domain.Repository;
using webapi_Domain.Entity;
using webapi_infrastructure.JsonHelper;


namespace webapi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        //测试接口
        private readonly IRepositoryBase<Users> _users;
        private readonly IRepositoryBase<Roles> _roles;

        private readonly ILogger<UserController> _logger;
        public UserController(ILogger<UserController> logger, IRepositoryBase<Users> users, IRepositoryBase<Roles> roles)
        {
            _logger = logger;
            _users = users;
            _roles = roles;
        }

        //整个表数据或者单条查找
        [HttpGet]
        public dynamic IndexUser(Guid Id)//两个功能所以这里不能是IEnumerable<> 只能是dynamic
        {
            if (Id.ToString().Replace("0", "").Length == 4)
            {
                //无数据则添加一条
                if (_users.Table.ToArray().Length == 0)
                {
                    _users.Add(new Users
                    {
                        Username = "admin",
                        Password = "123"
                    });
                }
                return _users.Table.ToList();
            }//查找单条数据
            else
            {
                return _users.Table.Where(x => x.Id == Id).FirstOrDefault();
            }
        }

        //参数模型类
        public class Model
        {
            public Guid Id { get; set; }
            public dynamic Username { get; set; }
            public string Password { get; set; }
        }

        [HttpPost]
        public dynamic AddUser(Model model)
        {
                var tmpuser =
                    new Users
                    {
                        Username = model.Username,
                        Password = model.Password
                    };
                _users.Add(tmpuser);

                return tmpuser;
        }

        [HttpPut]
        public Users UpdateUser(Model model)
        {
            var tempuser = _users.Table.Where(x => x.Id == model.Id).FirstOrDefault();
            if (tempuser != null)
            {
                tempuser.Username = model.Username;
                tempuser.Password = model.Password;
            }
            _users.Update(tempuser);
            
            return tempuser;
        }

        [HttpDelete]
        [Route("{Id}")]
        public Users RemoveUser(Guid Id)
        {
            var tempuser = _users.Table.Where(x => x.Id == Id).FirstOrDefault();
            if (tempuser != null)
            {
                _users.Deleted(Id);
            }
             _logger.LogInformation(tempuser.toJsonObject());
             return tempuser;
        }
        //只展示部分字段--不写默认为[HttpGet]
        public IEnumerable<dynamic> GetsomeList()
        {
            var temp = _users.Table.Select(x => new { x.Id, x.Username, x.Password }).ToList();
            return temp;
        }


        //为指定用户添加好友--不写默认为[HttpGet]
        [Route("{id}/friend")]
        public Model AddFriend(Guid Id, Model model)
        {
            this.AddUser(model);
            return model;
        }

        //查找指定用户好友--不写默认为[HttpGet]
        [Route("{id}/friend/{FId}")]
        public dynamic GetFriend(Guid Id, Guid FId)
        {
            return this.IndexUser(FId);
        }
    }
}
