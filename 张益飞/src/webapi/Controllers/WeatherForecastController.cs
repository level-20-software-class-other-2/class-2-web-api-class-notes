﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

//引入序列化自定义静态方法
using webapi_infrastructure.JsonHelper;
//手动序列化
using System.Text.Json;
//token
using webapi_infrastructure.CreateToken;
using Microsoft.AspNetCore.Authorization;


namespace webapi.Controllers
{   
    
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
      

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            _logger.LogInformation("成功进去Get方法,开始录入具体信息...");

            var rng = new Random();
            var res =  Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
            
            //自定义静态方法序列化
            // _logger.LogInformation(res.toJsonObject());

            //不用静态类, 直接手动序列化
            _logger.LogInformation(JsonSerializer.Serialize(res,new JsonSerializerOptions{WriteIndented=true}));
            return res;
        }
        //带token去发出请求
        [Authorize]
        [HttpPost]
        public dynamic Login(){             
            return "登入成功！我记得您，您在前1分钟内成功访问过我";
        }

        //拿到token
        [HttpDelete]
        public dynamic IsToken(){             
            var token = Token.GetToken();
            return token;
        }

        //测试全局异常过滤器
        [HttpPut]
        public dynamic IsExceptionFilter()
        {
            
            throw new Exception("我没有异常，我非常正常！");

            // return 200;
        }
    }
}












/* 日志写入文件的第二种方法配置
    "Serilog": {
        "MinimumLevel": {
            "Default": "Information",
            "Override": {
                "Microsoft": "Warning",
                "Microsoft.Hosting.Lifetime": "Information"
            }
        },
        "WriteTo": [{
            "Name": "File",
            "Args": { "path": "../LogFiles/log-.txt", "rollingInterval": "Day" }
        }]
    }
*/

