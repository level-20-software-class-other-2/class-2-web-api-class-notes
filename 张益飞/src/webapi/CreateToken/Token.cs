using System.Text;//Encoding
using System.Security.Claims;
using System;//生成令牌的构造函数expires参数需要DateTime
using Microsoft.IdentityModel.Tokens;//SymmetricSecurityKey、SigningCredentials、SecurityAlgorithms
using System.IdentityModel.Tokens.Jwt;//JwtSecurityToken、JwtSecurityTokenHandler
using System.Collections.Generic;

namespace webapi_infrastructure.CreateToken
{
    public class Token
    {
        public static string GetToken()
        {

        //所有参考复制关键字即可在bing搜索出文档
            //把18位字节打包成utf-8格式的秘钥原材料并生成原始秘钥
            // var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("{069BD1DF-72D8-474B-8950-2C3EB03B2D03}"));//tokenParameter.Secret
            // //加密秘钥-生成证书
            // var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            // //生成安全令牌 (构造函数有六个参数,四个可包含在claims中,一个是claims集合，一个是证书)
            // var jsToken = new JwtSecurityToken(null, null, null, expires:DateTime.UtcNow.AddMinutes(1), signingCredentials:creds);
            // //将JwtSecurityToken类型转换为string类型并去掉空格使其紧凑
            // var token = new JwtSecurityTokenHandler().WriteToken(jsToken);


                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("{069BD1DF-72D8-474B-8950-2C3EB03B2D03}"));
                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var token = new JwtSecurityTokenHandler().WriteToken( new JwtSecurityToken(
                    "lqc",
                    "Zyd",
                    new List<Claim>
                    {
                        new Claim(ClaimTypes.Role, "admin"),
                        new Claim(ClaimTypes.Gender,"Male"),
                        new Claim(ClaimTypes.Name,"zyf"),
                    },
                    expires: DateTime.UtcNow.AddMinutes(1),
                    signingCredentials: credentials
                ));

            return token;
        }
    }
}