using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

//引入数据库注入依赖的位置
using webapi_infrastructure.Db;
using Microsoft.EntityFrameworkCore;
//引入泛型依赖注入的位置
using webapi_Domain.Repository;
using webapi_infrastructure.Repository;
//引入审计日志过滤器
using webapi.AuditedAttribute;
//引入拦截请求的token
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using Microsoft.IdentityModel.Tokens;

//引入全局过滤器类
using webapi.Filter;


namespace webapi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //注入数据库依赖
            var constr = Configuration.GetConnectionString("SqlServer");//从appsettings.json中获取SqlServer
            
            services.AddDbContext<webDbContext>(options=>{
                options.UseSqlServer(constr);
            });
            //注入接口和实现类依赖--激活解析“webapi_Domain.Repository.IRepositoryBase`1[webapi_Domain.Entity.Users]”类型的服务
            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));


            //添加验证 使用jwt验证 
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = "lqc",

                        ValidateAudience = true,
                        ValidAudience = "Zyd",

                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("{069BD1DF-72D8-474B-8950-2C3EB03B2D03}")),

                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromMinutes(1),
                    };
                });

            //注入过滤器
            services.AddControllers(option => {
                //注入审计日志过滤器
                option.Filters.Add(typeof(AuditActionFilter));
                //注入全局异常过滤器
                 option.Filters.Add(typeof(ExceptionFilter));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            //使用授权
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
