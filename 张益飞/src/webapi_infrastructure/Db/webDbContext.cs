using Microsoft.EntityFrameworkCore;
using webapi_Domain.Entity;

namespace webapi_infrastructure.Db
{
    public class webDbContext : DbContext 
    {
        //为注入数据库依赖提供的构造函数
        public webDbContext(DbContextOptions<webDbContext> options): base(options)
        {

        }
        //这里应该是也要走，即使配置文件中不同也没事，最后都被这个给替换掉了！
        // private readonly string constr = "server=.;database=webapidatabase4;uid=sa;pwd=123456;";

        // protected override void OnConfiguring(DbContextOptionsBuilder builder)
        // {
        //     builder.UseSqlServer(constr);
        // }

        public DbSet<Users> Users {get;set;}
        public DbSet<Roles> Roles {get;set;}
        public DbSet<UserRoles> UserRoles {get;set;}
        public DbSet<AuditInfo> AuditInfo {get;set;}
        //记录异常的表
        public DbSet<ExceptionTable> ExceptionTable {get;set;}
    }
}