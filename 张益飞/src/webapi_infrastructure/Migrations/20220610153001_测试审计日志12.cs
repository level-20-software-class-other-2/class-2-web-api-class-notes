﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace webapi_infrastructure.Migrations
{
    public partial class 测试审计日志12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AuditInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsActived = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    UpdatedBy = table.Column<Guid>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    DisplayOrder = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Parameters = table.Column<string>(nullable: true),
                    BrowserInfo = table.Column<string>(nullable: true),
                    ClientName = table.Column<string>(nullable: true),
                    ClientIpAddress = table.Column<string>(nullable: true),
                    ExecutionDuration = table.Column<int>(nullable: false),
                    ExecutionTime = table.Column<DateTime>(nullable: false),
                    ReturnValue = table.Column<string>(nullable: true),
                    Exception = table.Column<string>(nullable: true),
                    MethodName = table.Column<string>(nullable: true),
                    ServiceName = table.Column<string>(nullable: true),
                    UserInfo = table.Column<string>(nullable: true),
                    CustomData = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditInfo", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuditInfo");
        }
    }
}
