using System.Text.Json;

namespace webapi_infrastructure.JsonHelper
{
    public static class JsonHelper
    {
        public static string toJsonObject(this object obj)
        {
            JsonSerializerOptions options =
                new JsonSerializerOptions
                {
                    WriteIndented = true
                };
            return JsonSerializer.Serialize(obj, options);
        }
    }
}

