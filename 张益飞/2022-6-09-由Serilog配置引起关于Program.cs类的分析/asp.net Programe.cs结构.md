# 一、asp.net Program类
```js
1 Program类包含的Main方法是ASP.NET Core应用的入口点

2 主要目的是 配置应用程序的基础结构 如：
     Web Host，日志记录，依赖注入容器，IIS集成等，
     它们是由 program类中Main方法的 createdefaultbuilder 方法创建，配置和构建的

2 webapi的Program启动时创建Web Host，然后 Web Host负责启动应用程序并运行它。它还可以配置日志记录、依赖注入容器、侦听HTTP请求的服务器等。  
```
#
#
# 二、webapi中Program.cs的组成
```c#
1 main方法

2 创建主机Host方法(main方法中被调用)


结构代码
    public class Program
    {
        public static void Main(string[] args)
        {
            //调用下面方法
            CreateHostBuilder(args).Build().Run();
        }
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            ...//此处省略了如何创建Host主机代码块,往下有图详解!
        }
    }
```
#
#
# 三、main方法中如何创建web host主机
# ![a](./images/0.png)
#
#
# 四、详解创建web host主机的CreateHostBuilder方法和内部调用的CreateDefaultBuilder方法
```c#
1 CreateHostBuilder方法
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            Host.CreateDefaultBuilder(args)...
                webBuilder.UseStartup<Startup>();//调用Startup.cs类完成主机配置(依赖注入、日志记录等)
            ...        
        }



2 CreateDefaultBuilder方法
    在CreateDefaultBuilder中执行以下操作:(只列了两个能大概理解的)
        1 加载可选配置启用记录--appsettings.json 等
            
        2 设置依赖项注入容器
```
#
#
## 补充一下之前为什么有 IHostBuilder和IWebHostBuilder 两种返回值类型的问题
#
# ![a](./images/1.png)
#
#
# 五、参考
https://www.cnblogs.com/yigegaozhongsheng/p/13373670.html


