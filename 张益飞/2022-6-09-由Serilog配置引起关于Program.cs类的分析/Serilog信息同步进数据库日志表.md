# 先给自己两巴掌，之前一点点侥幸，到最后一定会被积累放大
#
#
# 1、更改 appsettings.json 文件Serilog配置信息，将写入本地文件改为写入指定数据库
```c#
    "Serilog": {
        "MinimumLevel": "Information",
        "WriteTo": [{
            "Name": "MSSqlServer",
            "Args": {
                "connectionString": "server=.;database=webapidatabase9;uid=sa;pwd=123456;",
                "tableName": "Log",
                "autoCreateSqlTable": true //是否自动创建表，如果设置为ture，则在Serilog启动时检测数据库是否有对应的表，没有则创建
            }
        }]
    }


上面是最简单的连接数据库日志配置，Serilog为我们定义了一套标准：

    1 默认情况下会自动生成一些列。当然我们也可以自定义列,
            StandardColumn.Id 自增Id
            StandardColumn.Message 日志内容
            StandardColumn.MessageTemplate 日志模板
            StandardColumn.Level 等级
            StandardColumn.TimeStamp 记录时间
            StandardColumn.Exception 异常信息
            StandardColumn.Properties 日志事件属性值

    2 通常情况下还有
            columnOptions(日志表中的列定义)、
            batchPostingLimit(单次批量处理中提交的最大日志数量)、
            period(进行批量提交的间隔)等。
```
#
#
# 2、安装包(在基础设置项目中webapi_infrastructure)
```c# 
1 dotnet add package Serilog.Sinks.MSSqlServer

2 Serilog. Sinks.MSSqlServer是一个 定期批处理接收器 ,保在在最后处置记录器以强制它将日志刷新到数据库
```
#
#
# 3、只需要dotnet run 即可在数据库生成 Log 表 如图
# ![a](./images/2.png)
#
#
# 4、后面还可能使用到的一些功能
```
1 删除标准列
2 添加自定义列并把数据写入该列
3 对日志过滤(filter、Override、MinimumLevel)
4 Enricher
...等等
```
#
#
# 5、参考
```
1 https://www.cnblogs.com/fengchao1000/p/11811244.html
2 https://blog.csdn.net/WuLex/article/details/111823941
```
#
#
# 6、还有一个待尝试--能不能同时将日志输出到本地文件和数据库表?（感觉不难，一个突破口在于两个appsettings!）









