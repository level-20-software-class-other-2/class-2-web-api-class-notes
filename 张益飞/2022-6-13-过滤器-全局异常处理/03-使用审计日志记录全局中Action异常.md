# 一、这样做的优缺点
```c#
优点：
     1 可以省掉一张表模型



缺点：
     1 与审计日志合一张表，而表的字段又多，当数据量大的时候不方便排查异常

     2 只能记录发生在Action中的异常，Action之外的异常虽然能捕获但不会写入

     3 审计日志中Exception字段值不是经过ExceptionFilter类包装的异常信息,是最原始的一大片错误值
```
#
#
# 二、在AuditActionFilter类中把异常写入
```c#
namespace webapi.AuditedAttribute
{
    public class AuditActionFilter: IAsyncActionFilter
    {
        ....
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next) 
        {
            ....
            ActionExecutedContext result = null;
            try {
                result = await next();
                //执行过程因为代码问题产生的异常
                if (result.Exception != null && !result.ExceptionHandled) {
                    auditInfo.Exception = result.Exception.ToString();
                } 
            }catch (Exception ex) {
                    //系统异常--上面的if中写入出现的异常
                    auditInfo.Exception = ex.ToString();
                    throw;
            } finally {
                    ....
                    ....
            }
        }
    }   
}            
```
#
## 效果如图：
# ![a](./images/4.png)
#
# 
# 三、关于审计日志中的ActionExecutedContext类型 result 和 异常捕获中ExceptionContext类型 context区别
```c#

 1 result 是Action 执行完成后的返回信息体，context 是Action执行中的异常信息体，当有Action有异常的时候Action的返回值会被异常接口的返回值替换(这个返回值包含context)，所以当Action确实有异常时result包含了context



 2 由于result 并不等于context，所以不能误认为context.ExceptionHandled = true ,result.ExceptionHandled也同步成true



 3 面对同样的异常， result.Exception.ToString() 的值是完完全全未经过加工的一大串报错，而context.Exception.Message则是这一大串中最主要的错误信息

```
#
#
# 四、Startup.cs 自带的 UseDeveloperExceptionPage
```js
 1若没有写全局异常接口则异常都由它来捕捉
 
 
 2若写了它就基本只能成为审计日志的result.Exception.ToString()的值了^&^
```