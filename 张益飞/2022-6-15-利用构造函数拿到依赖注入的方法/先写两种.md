# 
# 1、注入后直接在类中拿到
```c#
 Startup.cs类中:

    public void ConfigureServices(IServiceCollection services)
    {
        ....

        //注入过滤器
        services.AddControllers(option => {
                //注入全局异常过滤器
                 option.Filters.Add(typeof(ExceptionFilter));
            });

        ....
    }
```
# 
#
# 2、从Service中拿到接口的依赖类型，再传给类的构造函数
```c#
 Startup.cs类中:

    public void ConfigureServices(IServiceCollection services)
    {
        ....
        //定义ServiceProvider对象
        var ServiceProvider=services.BuildServiceProvider();

        //通过ServiceProvider从Service中拿到接口的依赖类
        var IRep =ServiceProvider.GetService<IRepositoryBase<AppLogs>>();

        //注入全局异常过滤器
        services.AddControllers(options=>{
                options.Filters.Add(new MyGlobalExceptionFilter(IRep))
            });
        ....
    }
```
#
## 关于ServiceProvider对象，参考
https://www.cnblogs.com/artech/p/asp-net-core-di-service-provider-1.html
https://www.cnblogs.com/hippieZhou/p/11401267.html