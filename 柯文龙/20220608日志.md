## 时间：2022年6月8日

## Serilog

### 使用Serilog之前先得下载几个包,在vscore控制台并输入
```js
dotnet add package serilog.AspNetCore
```
### 通过Program.cs添加Serliog
```js
public class Program
    {
        public static int Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .CreateLogger();

            try
            {   
                Log.Information("aaa");
                Log.Information("Starting web host");
                CreateHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
             Host.CreateDefaultBuilder(args)
                .UseSerilog((context, services, configuration) => configuration
                    .ReadFrom.Configuration(context.Configuration)
                    .ReadFrom.Services(services)
                    .Enrich.FromLogContext()
                    .WriteTo.Console())
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}

```

### 在appsettings.json文件添加配置信息
```js
{
  "Serilog": {
    "MinimumLevel": {
      "Default": "Information",
      "Override": {
        "Microsoft": "Warning",
        "Microsoft.Hosting.Lifetime": "Information"
      }
    },
    "WriteTo": [
      {
        "Name": "File",
        "Args": { "path":  "./logs/log-.txt", "rollingInterval": "Day" }
      }
    ],
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "SqlServer":"server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;",
    "PostgreSQL":"server=host.9ihub.com;database=soft2_api_demo;uid=postgres;pwd=qq_112358;"
  }
}
}
```
### 在控制器写入
```js
 public IEnumerable<dynamic> Index(){
            _userRes.Add(new Users{
                Username="ok",
                Password="okk"
            });
            var list =  _userRes.Table.Select(x=>new {
                x.Id,
                x.Username,
                x.Password
            }).ToArray();
            var uuu = new List<dynamic>{
                new {
                    name="wd",
                    children=new List<dynamic>{
                        new {
                            kname="af"
                        }
                    }
                }
            };
            _logger.LogInformation(JsonSerializer.Serialize(list));
            return list;
        }
```
