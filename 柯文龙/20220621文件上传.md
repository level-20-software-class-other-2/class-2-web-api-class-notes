## 时间：2022年6月21日  文件上传

```
 [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private static IWebHostEnvironment _hostingEnv;
        public FileController(IWebHostEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;
        }

        [HttpPost]

        /// <summary>
                /// 上传图片
                /// </summary>
                /// <returns></returns>
        [HttpPost]
        public IActionResult UploadFile(IFormFile formFile)
        {
            //var formFile = Request.Form.Files[0];//获取请求发送过来的文件

            var webRootPath = Directory.GetCurrentDirectory();//_hostingEnv.WebRootPath;//应用程序根目录
            string dirPath = webRootPath + "/UploadFile/" + DateTime.Now.ToString("yyyyMMdd");
            //判断保存的文件夹是否存在，不存在创建
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            if (formFile != null)
            {
                //保存的文件名称可能存在重复，所以需要在后台对文件进行重命名
            string fileExtesion = Path.GetExtension(formFile.FileName);
            string fileName = Path.GetFileName(formFile.FileName) + "_" + Guid.NewGuid().ToString() + "." + fileExtesion;



                //保存文件
                using (var fs = System.IO.File.Create(dirPath + "/" + fileName))
                {
                    formFile.CopyTo(fs);
                }

                return new JsonResult(new { success = true, errMsg = "文件上传成功" });
            }
            else
            {
                return new JsonResult(new { success = false, errMsg = "未检测到上传的文件" });
            }

        }
```