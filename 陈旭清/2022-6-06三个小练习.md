###
### userscontroller
```c#
[Route("{id}/friends")]
        public IEnumerable<dynamic> GetFriends(Guid id)
        {
            var lst =
                new List<dynamic> {
                    new { Name = "猫先生", Age = 16 },
                    new { Name = "王雯雯", Age = 28 }
                };

            return lst;
        }
```
### api.http
```
### 指定用户的朋友列表
GET {{url}}/users/26B47F22-AB52-4E53-547D-08DA4531A005 HTTP/1.1

```

## 关于serilog日志文件

1.下载相关的安装包
```
dotnet add package Serilog.AspNetCore
dotnet add package Serilog.Sinks.File
dotnet add package Serilog.Sinks.Console
```

2.在program.cs文件中引入相关内容
```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
namespace Soft2ApiDemo.Api
{
    public class Program
    {
      
        public static void Main(string[] args)
        {
            //生成Log对象
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()    //输出的最小日志等级
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .WriteTo.Console()             //输出到控制台
                .WriteTo.File(AppContext.BaseDirectory + "/logs/log.log", rollingInterval: RollingInterval.Day)          //输出日志文件,并按日期分割
                .CreateLogger();               //创建Logger对象
 
            try
            {
                Log.Information("Starting up");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();            //重置全局设置
            }
        }
 
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(loggingBuilder => 
                { loggingBuilder.ClearProviders();     //去掉默认日志提供程序
                  loggingBuilder.AddSerilog();         //引用Serilog
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }


}

```
### WeatherForecastController.cs
在get的方法里面引入实例
```c#
_logger.LogInformation("TestLog",null);
```
```
3.最后在http://localhost:5000/WeatherForecast中查看所输出的日志文件

```