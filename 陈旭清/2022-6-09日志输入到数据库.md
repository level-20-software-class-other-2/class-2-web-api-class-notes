### 序列化和反系列化
```

二者实际上就是将数据持久化，防止一直存储在内存当中，消耗内存资源。而且序列化后也能更好的便于网络运输何传播

序列化：将java对象转换为字节序列

反序列化：把字节序列回复为原先的java对象
```
### 替换
```

public static void Main(string[] args)
{

    var connectionString = @"server=.;database=galaxy;uid=sa;pwd=123456;";//连接数据库字符串
    string tableNameA = "Logs";//输入日志的表名

               //连接数据库字符串 输入日志的表名 autoCreateSqlTable为true 时 会自动创建日志表
    Log.Logger = new LoggerConfiguration().WriteTo.MSSqlServer(connectionString,tableNameA, autoCreateSqlTable: true)
    //以下是控制台输出的信息
    .MinimumLevel
    .Override("Microsoft", LogEventLevel.Information)
    .Enrich
    .FromLogContext()
    .WriteTo
    .Console()
    .CreateLogger();

    try
    {
        Log.Information("开始Web宿主");
        CreateHostBuilder(args).Build().Run();
        // return 0;
    }
    catch (Exception ex)
    {
        Log.Fatal(ex, "Host terminated unexpectedly");
        // return 1;
    }
    finally
    {
        Log.CloseAndFlush();
    }

}
```

### 日志配置
```

public static IHostBuilder CreateHostBuilder(string[] args) =>
    Host
        .CreateDefaultBuilder(args)
        .UseSerilog()
        .ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder.UseStartup<Startup>();
        });
```