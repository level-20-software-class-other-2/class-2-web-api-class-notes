### webapi连接postgres数据库

postgres数据库与sqlserver数据库类似只需要改动一部分地方


第一个是infrastructure文件夹中的Db

数据库连接的账号密码及连接方式
```
private readonly string Connect ="server=www.galaxyeye.top;database=soft2uid=postgres;pwd=113";

protected override void OnConfiguring(DbContextOptionsBuilder builder)
{

    builder.UseNpgsql(Connect);
}
```

在次之前需要下载postgres数据库连接需要的东西,与sqlserver相同原理

```
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL -v 3.1
```

之后就可以数据迁移了

```
dotnet ef migrations add first
```

如果数据库中存在相同表会报错,当前数据库存在“xxx”表。要么把数据库存在的表删掉,要么改名