 # 进行调用
 ```cs
private string apiUrl = WebConfigurationManager.AppSettings[ "apiUrl" ].ToString( ); //接口地址放到webconfig配置<appSettings>中里

//查询
public ActionResult Index( )
{
    HttpClient client = new HttpClient( );
    client.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/json" ) );

    //执行Get操作
    HttpResponseMessage response = client.GetAsync( apiUrl ).Result;
    var list = response.Content.ReadAsAsync<List<Book>>( ).Result;
    
    return View(list);
}
//添加
[HttpPost]
public ActionResult AddBook(Book book )
{
    HttpClient client = new HttpClient( );
    client.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/json" ) );
    var  response = client.PostAsJsonAsync( apiUrl , book ).Result;

    return RedirectToAction( "Index" );
}
//删除
public ActionResult DeleteBook( int id )
{
    HttpClient client = new HttpClient( );
    client.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/json" ) );
    
    var response = client.DeleteAsync( apiUrl+id  ).Result;

    return RedirectToAction( "Index" );
}
//修改
[HttpPost]
public ActionResult UpdateBook( Book book )
{
    HttpClient client = new HttpClient( );
    client.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/json" ) );
    var response = client.PutAsJsonAsync( apiUrl , book ).Result;
    return RedirectToAction( "Index" );           
}
 ```