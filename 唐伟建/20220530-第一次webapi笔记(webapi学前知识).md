## 学前小知识

创建webapi

```
dotnet new webapi -n [名称] --no-https 
```

创建解决方案(solution)  .sln

```
dotnet new sln -n [名称] 
```

将webapi加入解决方案

```
dotnet sln add [webapi名称]
```

解决方案:解决方案是整个实现目的或目标的项目集成,然后逐一解决最后达成目的或目标



## git分支合并

创建一个项目

```
dotnet new webapi -n [xxx] --no-http 
```

添加忽略文件并本地初始化项目

```
git init
```

将文件添加到暂存区
```
git add .
```

将文件提交
```
git commit -m "测试"
```

![](./imgs/20220530191130.png)

查看日志

```
git logs
```
![](./imgs/20220530191300.png)

创建一个分支fixbug并查看日志

```
git checkout -b fixbug

git log
```

![](./imgs/20220530191827.png)

在fixbug分支下创建一个文件并提交

```
git add .
git commit -m "修复一个bug"
```
Test...为测试文件

![](./imgs/20220530192212.png)

返回master分支 查看日志 TestController.cs文件消失

```
git checkout master

git log
```

![](./imgs/20220530192516.png)


在master分支中合并fixbug分支修改内容
```
git merge fixbug
```
![](./imgs/20220530192719.png)

合并完成

