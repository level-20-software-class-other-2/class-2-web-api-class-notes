## 将日志输入到SqlServer数据库

在serilog程序日志可以实现的前提下，进行数据库输入。

前提就是运行之后控制台输出以下信息

![](./imgs/20220609173644.png)

第一步:

需要下载的包并需要在Program.cs文件中引入对应的包

```cs
dotnet add package Serilog.Sinks.MSSqlServer
```


在.Api文件夹的Program.cs文件中写入或替换以下代码


```cs
public static void Main(string[] args)
{

    var connectionString = @"server=.;database=galaxy;uid=sa;pwd=123456;";//连接数据库字符串
    string tableNameA = "Logs";//输入日志的表名

                                                        //连接数据库字符串 输入日志的表名 autoCreateSqlTable为true 时 会自动创建日志表
    Log.Logger = new LoggerConfiguration().WriteTo.MSSqlServer(connectionString,tableNameA, autoCreateSqlTable: true)
    //以下是控制台输出的信息
    .MinimumLevel
    .Override("Microsoft", LogEventLevel.Information)
    .Enrich
    .FromLogContext()
    .WriteTo
    .Console()
    .CreateLogger();

    try
    {
        Log.Information("开始Web宿主");
        CreateHostBuilder(args).Build().Run();
        // return 0;
    }
    catch (Exception ex)
    {
        Log.Fatal(ex, "Host terminated unexpectedly");
        // return 1;
    }
    finally
    {
        Log.CloseAndFlush();
    }

}
```


下面的日志配置只需要.UseSerilog()，即可。括号中不需要其他的东西。

```cs
public static IHostBuilder CreateHostBuilder(string[] args) =>
    Host
        .CreateDefaultBuilder(args)
        .UseSerilog()
        .ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder.UseStartup<Startup>();
        });
```


配置完就可以运行,数据库和控制台的情况如下:


![](./imgs/20220609175258.png)

![](./imgs/20220609175106.png)


接下就是控制台输出的信息都会存在数据库中