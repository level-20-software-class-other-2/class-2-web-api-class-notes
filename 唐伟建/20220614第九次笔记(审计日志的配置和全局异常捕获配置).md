## 审计日志的配置与使用

审计日志的作用:在访问端口的时候记录访问端口的用户和端口信息等等。

第一步:

创建审计日志信息表

在.Domain文件夹下的Entity文件夹下创建一个AuditInfo.cs 该表用来记录审计日志的信息。

AuditInfo.cs:

```cs
using System;

namespace Soft2ApiDemo.Domain.Entity
{
    public class AuditInfo : BaseEntity
    {
        /// <summary>
        /// 调用参数
        /// </summary>
        public string? Parameters { get; set; }
        /// <summary>
        /// 浏览器信息
        /// </summary>
        public string? BrowserInfo { get; set; }
        /// <summary>
        /// 客户端信息
        /// </summary>
        public string? ClientName { get; set; }
        /// <summary>
        /// 客户端IP地址
        /// </summary>
        public string? ClientIpAddress { get; set; }
        /// <summary>
        /// 执行耗时
        /// </summary>
        public int? ExecutionDuration { get; set; }
        /// <summary>
        /// 执行时间
        /// </summary>
        public DateTime ExecutionTime { get; set; }
        /// <summary>
        /// 返回内容
        /// </summary>
        public string? ReturnValue { get; set; }
        /// <summary>
        /// 异常对象
        /// </summary>
        public string? Exception { get; set; }
        /// <summary>
        /// 方法名
        /// </summary>
        public string? MethodName { get; set; }
        /// <summary>
        /// 服务名
        /// </summary>
        public string? ServiceName { get; set; }
        /// <summary>
        /// 调用者信息
        /// </summary>
        public string? UserInfo { get; set; }
        /// <summary>
        /// 自定义数据
        /// </summary>
        public string? CustomData { get; set; }
    }
}
```

表字段信息完成后去迁移数据和连接数据库，使得这个表存在在数据库中。


第二步:

在.infrastructure文件夹中的Filters文件夹下创建一个AuditLogActionFilter.cs文件

这个文件用于获取端口使用的各种信息和使用者并将数据插入到AuditInfo表中。

AuditLogActionFilter.cs

```cs
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using Soft2ApiDemo.Domain.Entity;//AuditInfo表所在位置
using Soft2ApiDemo.Domain.Repository;//增删改查方法实现类

namespace Soft2ApiDemo.Infrastructure.Filters
{
    public class AuditLogActionFilter : IAsyncActionFilter
    {
        private readonly IRepositoryBase<AuditInfo> _auditLogService;
        public AuditLogActionFilter(
            IRepositoryBase<AuditInfo> auditLogService
            )
        {
            _auditLogService = auditLogService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // 判断是否写日志
            if (!ShouldSaveAudit(context))
            {
                await next();
                return;
            }
            //接口Type
            var type = (context.ActionDescriptor as ControllerActionDescriptor).ControllerTypeInfo.AsType();
            //方法信息
            var method = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;
            //方法参数
            var arguments = context.ActionArguments;
            //开始计时
            var stopwatch = Stopwatch.StartNew();

            var auditInfo = new AuditInfo//数据对象
            {
                UserInfo = "管理员",//使用者的名字
                ServiceName = type != null ? type.FullName : "",
                MethodName = method.Name,
                ////请求参数转Json
                Parameters = JsonConvert.SerializeObject(arguments),
                ExecutionTime = DateTime.Now,
                BrowserInfo = context.HttpContext.Request.Headers["User-Agent"],
                ClientIpAddress = context.HttpContext.Connection.RemoteIpAddress.ToString(),
                //ClientName = _clientInfoProvider.ComputerName.TruncateWithPostfix(EntityDefault.FieldsLength100),
                // Id = Guid.NewGuid().ToString()
            };

            ActionExecutedContext result = null;

            try
            {
                result = await next();
                if (result.Exception != null && !result.ExceptionHandled)
                {
                    auditInfo.Exception = result.Exception.ToString();
                }
            }
            catch (Exception ex)
            {
                auditInfo.Exception = ex.ToString();
                throw;
            }
            finally
            {
                stopwatch.Stop();
                auditInfo.ExecutionDuration = Convert.ToInt32(stopwatch.Elapsed.TotalMilliseconds);

                if (result != null)
                {
                    switch (result.Result)
                    {
                        case ObjectResult objectResult:
                            auditInfo.ReturnValue = JsonConvert.SerializeObject(objectResult.Value);
                            break;

                        case JsonResult jsonResult:
                            auditInfo.ReturnValue = JsonConvert.SerializeObject(jsonResult.Value);
                            break;

                        case ContentResult contentResult:
                            auditInfo.ReturnValue = contentResult.Content;
                            break;
                    }
                }
                Console.WriteLine(auditInfo.ToString());
                //保存审计日志
                 _auditLogService.Add(auditInfo);
            }
        }

        /// <summary>
        /// 是否需要记录审计
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool ShouldSaveAudit(ActionExecutingContext context)
        {
            if (!(context.ActionDescriptor is ControllerActionDescriptor))
                return false;
            var methodInfo = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;

            if (methodInfo == null)
            {
                return false;
            }

            if (!methodInfo.IsPublic)
            {
                return false;
            }

            // if (methodInfo.GetCustomAttribute<AuditedAttribute>() != null)
            // {
            //     return true;
            // }

            // if (methodInfo.GetCustomAttribute<DisableAuditingAttribute>() != null)
            // {
            //     return false;
            // }

            // var classType = methodInfo.DeclaringType;
            // if (classType != null)
            // {
            //     if (classType.GetTypeInfo().GetCustomAttribute<AuditedAttribute>() != null)
            //     {
            //         return true;
            //     }

            //     if (classType.GetTypeInfo().GetCustomAttribute<AuditedAttribute>() != null)
            //     {
            //         return false;
            //     }
            // }
            return true;
        }
    }
}
```


第三步:

StartUp.cs的服务注册

```cs
using Soft2ApiDemo.Infrastructure.Filters;



    services.AddControllers(options =>
    {
        options.Filters.Add(typeof(AuditLogActionFilter));
    });
```

最后一步:测试审计日志效果

当成功运行时我们访问一下webapi自带的控制器WeatherForecast

数据库中的AuditInfo表便会有以下数据。

![](./imgs/20220614100135.png)

成功了！


## 全局异常捕获配置(无数据库信息输入)

在.infrastructure文件夹中的Filters文件夹下创建一个MyGlobalExceptionFilter.cs文件

MyGlobalExceptionFilter.cs:

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Soft2ApiDemo.Infrastructure.Filters
{
    public class MyGlobalExceptionFilter : IAsyncExceptionFilter
    {

        public Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.ExceptionHandled == false)
            {
                string msg = context.Exception.Message;
                context.Result =
                    new ContentResult {
                        Content = msg,
                        StatusCode = 200,
                        ContentType = "text/html;charset=utf-8"
                    };
            }
            context.ExceptionHandled = true; //异常已处理了

            return Task.CompletedTask;
        }
    }
}
```

你可能需要引入以下包

```cs
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
```

两个包下载:

`Microsoft.AspNetCore.Mvc.Abstractions`

`Microsoft.AspNetCore.Mvc`


配置完成后在StartUp.cs文件中注册服务:

```cs
using Soft2ApiDemo.Infrastructure.Filters;

services.AddControllers(options=>options.Filters.Add(new MyGlobalExceptionFilter()));
```

全局异常捕获的效果如下:

在webapi自带的控制器WeatherForecast中的端口抛出一个异常

```cs
throw new Exception("这是一个错误,位置是在WeatherForecast控制器中");
```

运行并访问网页

效果如下:

![](./imgs/20220614101853.png)

而没有全局异常捕获你将会看到

![](./imgs/20220614102019.png)