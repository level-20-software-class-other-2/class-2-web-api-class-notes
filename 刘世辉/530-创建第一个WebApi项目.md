# 第一节5.30  今天差点去洗脚了。


## WebApi是什么？
    WebAPI 是一种用来开发系统间接口、设备接口 API 的技术，基于 Http 协议，请求和返 回格式结果默认是 json 格式。比 WCF 更简单、更通用，比 WebService 更节省流量、更简洁。

## 创建第一个WebApi项目 我是用的vs，不是vs code，，所以如下：


![](./img/530-1.png)
![](./img/530-2.png)
记得把https的√取消了
![](./img/530-3.png)

## WebApi文件调试
按f5，再f12，再网络选项里观察页面的内容它是json文件
![](./img/530-4.png)
