## 元包
此处的元包值得是由 Microsoft 开发并提供支持的一组程序集——ASP.NET Core 共享框架(Microsoft.AspNetCore.App),   
当安装 `.Net 3.0` 及其以上版本的 SDK 时， 安装程序会自动安装。 共享框架是安装在计算机上并包括运行时组件和目标包的一组   
动态链接库(.dll 文件) 在官网的文档中通常被称为程序集， 该程序集不包括第三方依赖项。

## 为什么要使用元包
```
有一些项目， 比如说 web api 之类的面向 Microsoft.NET.Sdk.Web 的项目依赖于这个包， 如果没有这个， 项目就无法正常运作，  
并且这些项目在隐式的引用了这个包， 但是某些项目（也就是 SDK != Microsoft.NET.Sdk.Web 的项目）， 程序并不能判断这个项目一定需要这个包，   
比如说 类库（classlib)……， 如果我们需要， 那可以显示的指定对这个包的引用:
```

``` xml
<!-- Sdk 并不一定要是这个， 只要不是 Microsoft.NET.Sdk.Web 就行，
     因为 Microsoft.NET.Sdk.Web 隐式引用了这个包， 不需要在显式的引用 -->
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <!-- TargetFramwork 必须大于等于 3.0 因为 3.0 以下的版本包是不同的 -->
    <TargetFramework>net6.0</TargetFramework>
    <!-- 这玩意是 .Net 6 的新特性， 设置一些命名空间的全局引用，   
          这会减少一些文件中的对常用命名空间的引入， 默认具有的引用位于
          {project_path}/obj/Debug/net6.0/tmp.GlobalUsings.g.cs 文件  -->
    <ImplicitUsings>enable</ImplicitUsings>
    <!-- 这个是设置某些字段是否为空， 在 6.0 中启用这个东西以后可以按如下形式
         int? variable_name 来设置该变量的值可以为空或者 int -->
    <Nullable>enable</Nullable>
  </PropertyGroup>

  <ItemGroup>
    <!-- 引入元包 -->
    <FrameworkReference Include="Microsoft.AspNetCore.App" />
  </ItemGroup>

</Project>
```