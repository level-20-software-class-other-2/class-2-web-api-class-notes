# 20220607 天气：雨 心情：无
# .NetWebapi 日志文件
## 一、安装Serilog日志文件
1. 下载相关安装包
+ dotnet add package Serilog.AspNetCore
+ dotnet add package Serilog.Sinks.File
+ dotnet add package Serilog.Sinks.Console
## 二、引入相关内容
```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
namespace Soft2ApiDemo.Api
{
    public class Program
    {
      
        public static void Main(string[] args)
        {
            //生成Log对象
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()    //输出的最小日志等级
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .WriteTo.Console()             //输出到控制台
                .WriteTo.File(AppContext.BaseDirectory + "/logs/log.log", rollingInterval: RollingInterval.Day)          //输出日志文件,并按日期分割
                .CreateLogger();               //创建Logger对象
 
            try
            {
                Log.Information("Starting up");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();            //重置全局设置
            }
        }
 
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(loggingBuilder => 
                { loggingBuilder.ClearProviders();     //去掉默认日志提供程序
                  loggingBuilder.AddSerilog();         //引用Serilog
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }


}
```
## 三、在get方法里引入实例
```c#
_logger.LogInformation("TestLog",null)
```