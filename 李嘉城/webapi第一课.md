# 2022-05-30 webapi第一课

## 创建解决方案
```
dotnet new sln -n soft2
```

## 创建项目
```
dotnet new webapi -o soft2.api
```

## 把项目添加到解决方案
```
dotnet sln add 项目名称
```


## 运行项目
```
cd 到指定文件夹(项目文件夹)
dotnet run
```