# 2022-06-01 WebApi第二课

## 生成文件
```
dotnet new webapi -o soft2.api
dotnet new classlib -o soft2.Domain
dotnet new classlib -o soft2.inst
dotnet new sln -n soft2

```

## 把文件添加到解决方案里
```
dotnet sln add soft2.api
dotnet sln add soft2.Domain
dotnet sln add soft2.inst
```

## 创建表模型
soft2.Domain
```js
BaseEntity
using System;
namespace soft2.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public bool Istived { get; set; }
        public bool Isdelete { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }

        public int Orderby { get; set; }
        public String Remark { get; set; }
    }
}
=========
Users
using System;
namespace soft2.Domain.Entity
{
    public class Users:BaseEntity
    {
        public string UserName {get;set;}
    }
}
=========
Roles
using System;
namespace soft2.Domain.Entity
{
    public class Roles:BaseEntity
    {
         public string RoleName {get;set;}
    }
}
=========
UserRoles
using System;
namespace soft2.Domain.Entity
{
    public class UserRoles:BaseEntity
    {
        public Guid UserId{get;set;}
        public Guid RoleId{get;set;}
        public Users User{get;set;}
        public Roles Role{get;set;}
    }
}
```

## 下载包
```
dotnet add package Microsoft.EntityFrameworkCore -v 3.1
dotnet add package Microsoft.EntityFrameworkCore.SqlServer -v 3.1
dotnet add package Microsoft.EntityFrameworkCore.Design -v 3.1
dotnet tool install -global dotnet-ef
```

## 迁移文件更新数据库
```
dotnet ef migrations add 88 -s ..\soft2.api\
dotnet ef database update -s ..\soft2.api\

```