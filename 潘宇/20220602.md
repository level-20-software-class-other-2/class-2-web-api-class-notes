# 2022-06-02

## 在soft2.Domain里面定义接口
```cs
using System;
using System.Linq;

namespace soft2.Domain.Repository
{
    //使用接口
    public interface BaseRepository<T>
    {
       T GetByid (Guid T);
        IQueryable<T> Table{get;}
        void Add(T Entity);
        void Update(T Entity);
        void Delete(Guid id);
    }
}
```
## 在soft2.inst里面实现接口
```cs
//引入
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using soft2.Domain.Repository;
using soft2.inst.Db;
using Soft2ApiDemo.Domain;
//命名空间
namespace soft2.inst.Repository
{
    //使用泛型
    public class IRepository<T> : BaseRepository<T> where T : BaseEntity
    {   
        //定义db 和 table
        private readonly soft2Db _db;
        private DbSet<T> _Table;
        //定义有参的构造方法
        public IRepository(soft2Db db)
        {
            _db = db;
            _Table = _db.Set<T>();
        }
        public IQueryable<T> Table
        {
            get
            {   
            //判断表是否为空如果为空则DbSet初始化
                if (_Table == null)
                {
                    _Table = _db.Set<T>();
                }
                //返回
                return _Table.AsNoTracking().AsQueryable<T>();
            }
        }
        //实现新增方法
        public void Add(T Entity)
        {
            _db.Add(Entity);
            _db.SaveChanges();
        }
        //实现删除

        public void Delete(Guid id)
        {
            _db.Remove(id);
            _db.SaveChanges();
        }

        //实现查询id方法
        public T GetByid(Guid id)
        {
            var entity = _Table.Where(x=>x.Id==id).FirstOrDefault();
            return entity;
        }
        //实现更新

        public void Update(T Entity)
        {
            _db.Update(Entity);
            _db.SaveChanges();
        }
    }
}
```