## 文件上传

第一步:

在.infrastructure文件夹中创建一个名为Files的文件夹，并在该文件夹下创建一个名为AanFiles.cs的文件

AanFiles.cs
```cs
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Soft2ApiDemo.Domain.Entity;
using Soft2ApiDemo.Domain.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Soft2ApiDemo.Infrastructure.Files
{
    public class AntFiles
    {
        private readonly IRepositoryBase<UploadFileInfo> _fileInfoRepository;

        public AntFiles(IRepositoryBase<UploadFileInfo> fileInfoRepository)
        {
            _fileInfoRepository = fileInfoRepository;
        }

        /// <summary>
        /// 批量上传文件（主要用于图片）
        /// </summary>
        /// <param name="inputs">上传的文件集合</param>
        /// <param name="saveFilePath">保存文件到指定的目录，可以不传使用默认值</param>
        /// <returns></returns>
        public IEnumerable<Guid> UploadFiles(IFormCollection inputs, string saveFilePath = "UploadFiles/Images")
        {
            // 获得当前应用所在的完整路径（绝对地址）
            var filePath = Directory.GetCurrentDirectory();

            // 最终存放文件的完整路径
            var preFullPath = Path.Combine(filePath, saveFilePath);
            // 如果路径不存在，则创建
            if (!Directory.Exists(preFullPath))
            {
                Directory.CreateDirectory(preFullPath);
            }

            var resultPath = new List<string>();
            var uploadfiles = new List<UploadFileInfo>();

            
            

            foreach (IFormFile file in inputs.Files)
            {

                if (file.Length > 0)
                {

                    var fileName = file.FileName;
                    var extName = fileName.Substring(fileName.LastIndexOf("."));//extName包含了“.”
                    var rndName = Guid.NewGuid().ToString("N") + extName;
                    var tempPath = Path.Combine(saveFilePath, rndName).Replace("\\", "/");
                    
                    using (var stream = new FileStream(Path.Combine(filePath, tempPath), FileMode.CreateNew))//Path.Combine(_env.WebRootPath, fileName)
                    {
                        file.CopyTo(stream);
                    }

                    var tmpFile = new UploadFileInfo
                    {
                        OriginName = fileName,
                        CurrentName = rndName,
                        RelativePath = tempPath,
                        ContentType = file.ContentType
                    };

                    uploadfiles.Add(tmpFile);

                    // 此处地址可能带有两个反斜杠，虽然也能用，比较奇怪，统一转换成斜杠，这样在任何平台都有一样的表现
                    resultPath.Add(tempPath.Replace("\\", "/"));
                }
            }
            
            // 批量插入数据库
            _fileInfoRepository.AddBulk(uploadfiles);

            // 选择上传文件后生成的id
            var ids = uploadfiles.Select(x => x.Id);

            return ids;
        }


        /// <summary>
        /// 根据文件Id返回字节数组（字节流）
        /// </summary>
        /// <param name="id">文件Id</param>
        /// <returns></returns>
        public FileContentResult ViewFile(Guid id)
        {
            var currentPath = Directory.GetCurrentDirectory();
            var fileInfo = _fileInfoRepository.GetById(id);
            // 如果数据库没有找到对应的文件记录，则返回空内容
            if (fileInfo == null)
            {
                return new FileContentResult(new byte[0], "image/jpeg");
            }
            var fullPath = Path.Combine(currentPath, fileInfo.RelativePath);

            // 如果在当前文件系统中没有找到文件，则返回空内容
            if (!System.IO.File.Exists(fullPath))
            {
                return new FileContentResult(new byte[0], "image/jpeg");
            }

            using (var sw = new FileStream(fullPath, FileMode.Open))
            {
                var contenttype = fileInfo.ContentType;
                var bytes = new byte[sw.Length];
                sw.Read(bytes, 0, bytes.Length);
                sw.Close();
                return new FileContentResult(bytes, contenttype);
            }
        }
    }
}
```


第二步:

在.Domain文件夹中的Entity文件夹内添加一个UploadFileInfo.cs的表文件

UploadFileInfo.cs

```cs
namespace Soft2ApiDemo.Domain.Entity
{
    public class UploadFileInfo : BaseEntity
    {

        // 原始名称（含扩展名）
        public string OriginName { get; set; }

        // 现在的名称（随机名称，含扩展名）
        public string CurrentName { get; set; }

        // 相对地址
        public string RelativePath { get; set; }

        public string ContentType{get;set;}

    }
}
```

`接下来就迁移数据并连接数据库`


第三步:

在.Api文件夹中的控制器中添加构造方法

```cs   
using Microsoft.AspNetCore.Http;
using Soft2ApiDemo.Infrastructure.Files;

        //上传文件
        [HttpPost]
        [Route("file")]
        public string UploadFile(IFormCollection inputs)
        {
            
            var saveFilePath = _configuration["UploadFilesPath"];
            Console.WriteLine(saveFilePath);
            var antFiles = new AntFiles(_fileInfoRepository);
            var ids = antFiles.UploadFiles(inputs, saveFilePath);

            var result = new
            {
                Code = 1000,
                Data = ids,
                Msg = "文件/图片上传成功！"
            };

            return JsonHelper.SerializeObject(result);
        }
        //根据Id查上传文件的信息
        [HttpGet,Route("{id}")]
        public FileContentResult Get(Guid id)
        {
            var antFiles = new AntFiles(_fileInfoRepository);
            var fileContent =antFiles.ViewFile(id);
            return fileContent;
        }
```

最后一步测试文件上传:

根据一位同学的推荐我们使用前端上传文件

```html
<!DOCTYPE html>
<html>
<head>
    <title>.Net Core WebApi图片上传</title>
    <meta charset="utf-8"/>
    <script type="text/javascript" src="./jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.form.min.js"></script>
</head>
<body>
<h1>通过form表单提交</h1>
<form id="myform" name="myform" method="post" enctype="multipart/form-data" 
                    action="https://localhost:44376/Upload/FormImg">
    <input type="file" name="files" id="files" value="选择需要上传的文件" multiple />
    <input type="button" id="submitbtn" value="提交" onclick="uplpadfile()">
</form>

<div>
   上传的图片，返回的地址
   <div id="imglist">
</div>

</div>

<script type="text/javascript">

//前端第一种提交方式
    function  uplpadfile(){
        //获取表单的数据        
        var formdata
        var file = $("#files").get(0);
        var files = file.files;
        var formdata = new FormData();
        for (var i = 0; i < files.length; i++) {
            formdata.append("files", files[i]);
        }

        $.ajax({
            type:'Post',
            data:formdata,                        
            contentType: false,
            processData: false,
            url:"http://localhost:5000/users/file",
            success:function(result){
                if (result.Success) {
                    var imglist =result.Data; 
                    for(var i in imglist){
                        $("#imglist").append('<img src="'+imglist[i]+'"/>');
                    }                    
                }else{                    
                    alert('提交失败,重新尝试提交');
                }                            
            }
        })


    };
</script>

</body>
</html>

```
