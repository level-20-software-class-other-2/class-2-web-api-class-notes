## 数据迁移

第一步 : 创建四个主要文件一个webapi一个sln两个classlib

创建webapi文件

```
dotnet new webapi --no-https -n ApiDemo.Api
```

创建sln文件

```
dotnet new sln -n ApiDemo
```


创建两个classlib文件 分别是Domain和infrastructure

```
dotnet new classlib -n ApiDemo.Domain

dotnet new classlib -n ApiDemo.infrastructure
```

文件创建成功后将三个文件写入到解决方案中
```
dotnet sln add .\ApiDemo.Api\ .\ApiDemo.Domain\ .\ApiDemo.infrastructure\
```

第二步 : 在Domain文件中写数据库字段 分两部分 BaseEntity.cs文件 和 Entity文件夹

BaseEntity文件负责所有字段公共部分,为之后继承给主要表

```cs
using System;

namespace apidemo.Domain
{
    public abstract class BaseEntity
    {
        // 主键
        public Guid Id { get; set; }

        // 标识是否启用
        public bool IsActived { get; set; }

        // 是否删除（伪删除、软删除）
        public bool IsDeleted { get; set; }

        // 创建者（的Id）
        public Guid CreatedBy { get; set; }

        // （最后）修改者（的Id）
        public Guid UpdatedBy { get; set; }

        // 记录的创建时间
        public DateTime CreatedAt { get; set; }

        // （最后）更新时间
        public DateTime UpdatedAt { get; set; }

        // 展示顺序，默认为0
        public int DisplayOrder { get; set; }

        // 备注
        public string Remarks { get; set; }

    }
}
```
schools.cs
```cs
using System;

namespace apidemo.Domain.entity
{
    public class Schools : BaseEntity
    {
        public string SchoolName { get; set; }

    }
}
```
students.cs
```cs
using System;

namespace apidemo.Domain.entity
{
    public class Students : BaseEntity
    {
        public string Name { get; set; }
        public int Age { get; set; }
        
    }
}
```
stuschools.cs
```cs
using System;

namespace apidemo.Domain.entity
{

    public class StuSchool : BaseEntity
    {
        public Guid StudentId { get; set; }
        public Guid SchoolId { get; set; }

        public Schools school { get; set; }

        public Students student { get; set; }


    }
}
```

第三步 : 在infrastructure文件夹中写数据库上下文并生成数据迁移

在infrastructure文件夹中创建Db文件夹,并创建ApiDbContext.cs文件

在这里需要下载EntityFrameworkCore和EntityFrameworkCore.SqlServer

    dotnet add package Microsoft.EntityFrameworkCore -v 3.1

并且需要引入Domain文件

当你在infrastructure文件夹路径下时可以使用以下代码

```
dotnet add reference ..\ApiDemo.Domain\
```

`非infrastructure文件夹路径下`

```
dotnet add ..\ApiDemo.infrastructure\ reference ..\ApiDemo.Domain\
```

```cs
using Microsoft.EntityFrameworkCore;
using apidemo.Domain.entity;

namespace apidemo.infrastructrue.Db
{
    public class ApiDbContext : DbContext
    {
        private readonly string sql = "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;";
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(sql);
        }

        public DbSet<Schools> Schools{ get; set; }
        public DbSet<Students> Students { get; set; }
        public DbSet<StuSchool> StuSchools { get; set; }

    }
}
```

当完成以上步骤时就可以编译一下查看是否存在语法错误或是步骤遗漏操作失误等等。

如果编译成功就可以最后一步生成迁移文件(警告可以忽略)

最后一步 : 生成迁移文件

下载ef工具

```
 dotnet tool install --global dotnet-ef --version 3.1
```

路径切换到Api文件夹下载Microsoft.EntityFrameworkCore.Design

```
cd ..\ApiDemo.Api\

dotnet add package Microsoft.EntityFrameworkCore.Design -v 3.1
```

在Api文件夹路径下引入infrastructure文件夹

```
dotnet add reference ..\ApiDemo.infrastructure\
```

`不在Api文件夹路径下`

```
dotnet add ..\ApiDemo.Api\ reference ..\ApiDemo.infrastructure\
```

最后

```
dotnet ef migrations add [自定义名称] -s ..\ApiDemo.Api\

-s 设置启动项startup-project
```

不设置启动项会报以下错误

```r
Your startup project 'soft2WebApi.infrastructure' doesn't reference Microsoft.EntityFrameworkCore.Design. This package is required for the Entity Framework Core Tools to work. Ensure your startup project is correct, install the package, and try again.
```

最后的最后(如果顺利)就可以看见Migrations文件夹并且文件夹中的文件存在字段数据。

连接数据库

dotnet ef database update -s ..\ApiDemo.Api\