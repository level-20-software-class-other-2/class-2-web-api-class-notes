## 2022.05.31   星期二    噩耗


### 一些命令

> 安装ef
+ dotnet tool install -g dotnet-ef

+ dotnet add package Microsoft.EntityframeworkCore -v 3.1

+ dotnet add package Microsoft.EntityframeworkCore.SqlServer -v 3.1

+ dotnet add package Microsoft.EntityframeworkCore.Design -v 3.1

+ dotnet add reference ../DemoSoft.Domain/

> 生成迁移文件

+ dotnet ef migrations add [自定义名称] -s ..\soft2WebApi.Api\

> (-s 设置启动项startup-project)

## 创建 Demo.Domain
> Baseentity.cs
```js
using System;
namespace Demo.Domain
{
    public abstract class BaseEntity
    {
        public string Id { get; set; }
        
        public string CreateBy { get; set; }

        public string displayOrder { get; set; }

        public string Remark { get; set; }
    }
}

```
> users.cs
```js
namespace Demo.Domain.Entity
{
    public class Users : BaseEntity
    {
        public string userName { get; set; }
    }
}

```
> roles.cs
```js
namespace Demo.Domain.Entity
{
    public class roles : BaseEntity
    {
        public string roleName { get; set; }
    }
}

```
> Usersroles.cs 
```js
namespace Demo.Domain.Entity
{
    public class UserRoles : BaseEntity
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public Users User { get; set; }
        public roles Role { get; set; }
    }
}

```

