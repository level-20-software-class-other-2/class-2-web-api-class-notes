﻿## 在当前文件夹创建项目
```
dotnet new webapi --no-https
```

## 在当面文件夹重新创建指定项目文件夹
```
dotnet new webapi -n 文件夹名 --no-https
```

## 添加解决方案

```
dotnet new sln -n 项目名
dotnet sln add -n 项目名
```

## 忽略文件夹提交中暂存区.gitignore

```
.vscode
bin
obj
```

## git提交缓存区相关操作

## 提交缓存区

```
git init 
git add .
git commit -m "提交信息"

```

## 查看提交记录
```
git log
```

## 返回提交记录中的上一步
```
git reset  HEAD^
```