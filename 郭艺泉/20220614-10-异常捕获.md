# 20220614-10-异常捕获

---

## 全局异常捕获数据输入数据库

第一步:

在.Domain文件夹中的Entity文件夹下创建一个接收捕获异常数据的表比如:

```
AppLogs
namespace Soft2ApiDemo.Domain.Entity
{
    public class AppLogs : BaseEntity
    {
        public string Message { get; set; }//报错的异常信息

        public string Exception { get; set; }//异常类型
    }
}
```

在数据库上下文中创建实例并且数据迁移和连接数据库。

第二步:

在.infrastructure文件夹中的Filters文件夹下创建一个全局异常捕获的.cs文件

```
MyGlobalExceptionFilter.cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

using Soft2ApiDemo.Domain.Entity;//AppLogs表的引入
using Soft2ApiDemo.Domain.Repository;//增删改查的方法引入

namespace Soft2ApiDemo.Infrastructure.Filters
{
    public class MyGlobalExceptionFilter : IAsyncExceptionFilter
    {
        private readonly IRepositoryBase<AppLogs> _logsRes;//申明AppLogs表

        public MyGlobalExceptionFilter(IRepositoryBase<AppLogs> logsRes){
            _logsRes=logsRes;//返回表实例
        }

        public Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.ExceptionHandled == false)
            {

                string msg = context.Exception.Message; //异常数据
                var exc = context.Exception.Tostring(); //异常类型(需要tostring才可以赋给var或string变量)

                _logsRes.Add(new AppLogs{
                    Message=msg,  //异常数据
                    Exception=exc //异常类型
                });

                context.Result =
                    new ContentResult {
                        Content = msg,
                        StatusCode = 200,
                        ContentType = "text/html;charset=utf-8"
                    };
            }
            context.ExceptionHandled = true; //异常已处理了

            return Task.CompletedTask;
        }
    }
}
```

第三步:

在StartUp.cs文件中注册服务

办法一:

```
var provider=services.BuildServiceProvider();

var IRep =provider.GetService<IRepositoryBase<AppLogs>>();         

            
services.AddControllers(options=>options.Filters.Add(new MyGlobalExceptionFilter(IRep)));
```

办法二:

```
services.AddControllers(options=>options.Filters.Add(typeof(MyGlobalExceptionFilter)));
```

## dotnet自定义模板

第一步:

随便创建一个文件夹

在文件夹下创建项目文件夹和.tamplate.config文件夹

src文件夹是项目的内容

重点是.tamplate.config文件夹,在该文件夹下创建template.json文件

```
{
    "author": "Mr.T",                    //此模板的作者名称
    "classifications": [ "Web/WebAPI" ], //模板分类，出现在模板的Tags列中
    "name": "soft2webapi",               //模板名称
    "identity": "soft2Template",         //模板唯一名
    "shortName": "swb",                  //模板名称的简写
    "tags": {
      "language": "C#" ,                 //项目语言
      "type":"project"                   //项目类型
    },
    "sourceName": "soft2Template",       //这是一个可选参数，我们在执行dotnet new的时候，可以指定-n（name）参数，这个参数定义了项目名称，在指定-n的情况下，该设置的字符串会被其替代。
    "preferNameDirectory": true          //：在执行dotnet new的时候，如果你有指定-n参数，预设会建立一个与-n参数值同名的文件夹，如果你将该属性设定为false的话，就不会创建。此属性默认为true。
}
```

- author：此模板的作者名称
- identity：模板唯一名
- name：模板名称
- shortName：模板名称的简写
- classifications：模板分类，出现在模板的Tags列中
- sourceName：这是一个可选参数，我们在执行dotnet new的时候，可以指定-n（name）参数，这个参数定义了项目名称，在指定-n的情况下，该设置的字符串会被其替代。
- preferNameDirectory：在执行dotnet new的时候，如果你有指定-n参数，预设会建立一个与-n参数值同名的文件夹，如果你将该属性设定为false的话，就不会创建。此属性默认为true。
- 

第二步:

进入项目根目录（当然您也可以在-i后面自己指定.template.json文件夹路径），安装示例如下

```
dotnet new -i .
```

安装完毕后可以使用以下代码查看是否安装成功

```
dotnet new -l
```

改代码可以显示当可以创建模板