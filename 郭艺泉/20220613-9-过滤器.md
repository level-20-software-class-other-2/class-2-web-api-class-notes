# 20220613-9-过滤器

---

### 过滤器
在某些特定的时候， 我们希望将一些错误给写入到对应的数据库中， 在 .Net 中我们可以使用异常过滤器来实现这个功能，
 我们需要实现 IAsyncExceptionFilter 接口中的 OnExceptionAsync 方法，在默认情况下所有的异常都会触发这个函数
## 示例
```
dotnet add .\Soft2ApiDemo.Infrastructure\ package Microsoft.AspNetCore.Mvc.Abstractions
dotnet add .\Soft2ApiDemo.Infrastructure\ package Microsoft.AspNetCore.Mvc
```

Soft2ApiDemo.Api文件夹下Controllers文件夹下UsersController.cs

```
using System;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Soft2ApiDemo.Domain.Entity;
using Soft2ApiDemo.Domain.Repository;
using Soft2ApiDemo.Infrastructure.Utils;

namespace Soft2ApiDemo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IRepositoryBase<Users> _usersRes;

        public UsersController(IRepositoryBase<Users> usersRes,ILogger<UsersController> logger)
        {
            _logger=logger;
            _usersRes = usersRes;
        }

        [HttpGet]
        public IEnumerable<dynamic> Index()
        {
            throw new Exception("这是一个错误，延续了一个世纪的错误");
            _usersRes
                .Add(new Users {
                    Username = "老胡来也",
                    Password = "没有密码"
                });
            var lst =
                _usersRes
                    .Table
                    .Select(x =>
                        new {
                            x.Id,
                            x.Username,
                            x.Password,
                            Nickname = "柯文龙不要睡觉"
                        })
                    .ToArray();
            var uuu =
                new List<dynamic> {
                    new {
                        Name = "天天向上",
                        Age = "3389",
                        Sex = "男",
                        Children =
                            new List<dynamic> {
                                new { KidName = "大娃", Age = 1 },
                                new { KidName = "二娃", Age = 0.9 }
                            }
                    },
                    new {
                        Nickname = "你的童年我的童年，好像都一样",
                        Height = 1990
                    }
                };
                _logger.LogInformation( uuu.SerializeObject());
            return uuu;
        }

        [HttpPost]
        public Users AddUser(UserCto model)
        {
            var tmp =
                new Users {
                    Username = model.Username,
                    Password = model.Password
                };
            _usersRes.Add (tmp);
            return tmp;
        }

        public class UserCto
        {
            public string Username { get; set; }

            public string Password { get; set; }
        }

        [HttpPut]
        [Route("{id}")]
        public Users UpdateUser(Guid id, UserCto model)
        {
            var entity = _usersRes.GetById(id);
            if (entity != null)
            {
                entity.Password = model.Password;
                _usersRes.Update (entity);
            }
            return entity;
        }

        // [HttpDelete]
        [Route("{id}")]
        public dynamic DeleteUser(Guid id)
        {
            _usersRes.Delete (id);
            return new { Code = 1000, Msg = "删除成功", Data = "" };
        }

        [HttpPost]
        [Route("{id}/friends")]
        public dynamic GetFriends(Guid id, UserCto model)
        {
            var lst =
                new {
                    Code = 1000,
                    Msg = "为指定用户新增朋友成功",
                    Data = new { Id = id, Model = model }
                };
            return lst;
        }

        [Route("{id}/friends")]
        public IEnumerable<dynamic> GetFriends(Guid id)
        {
            var lst =
                new List<dynamic> {
                    new { Name = "唐大腿", Age = 16 },
                    new { Name = "王文文", Age = 28 }
                };

            return lst;
        }

        [Route("{id}/friends/{friendId}")]
        public dynamic GetFriends(Guid id, Guid friendId)
        {
            var lst =
                new {
                    Code = 1000,
                    Msg = "成功",
                    Data = new { Name = "唐大腿", Age = 16 }
                };

            return lst;
        }
    }
}
```

Soft2ApiDemo.Api文件夹下Startup.cs

```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Infrastructure.Db;
using Soft2ApiDemo.Domain.Repository;
using Soft2ApiDemo.Infrastructure.Repository;
using Soft2ApiDemo.Infrastructure.Filters;

namespace Soft2ApiDemo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var conString = Configuration.GetConnectionString("SqlServer");

            // IoC 依赖倒转
            // DI  依赖注入

            // DI分两大步，第一步叫注册，注册服务接口和具体实现到容器
            // 第二步，其实就是真正在使用的时候，注入服务到构造函数、属性、特性
            services
                .AddDbContext<Soft2ApiDemoDbContext>(options =>
                {
                    options.UseSqlServer(conString);
                });

            services.AddScoped(typeof(IRepositoryBase<>),typeof(RepositoryBase<>));

            
            services.AddControllers();
            // services.AddControllers(options=>options.Filters.Add(new MyGlobalExceptionFilter()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }
    }
}
```

Soft2ApiDemo.Api文件夹下appsettings.json

```
{
  "Serilog": {
    "Using": [
      "Serilog.Sinks.MSSqlServer","Serilog.Sinks.Console"
    ],
    "MinimumLevel": "Information",
    "WriteTo": [
      {
        "Name": "MSSqlServer",
        "Args": {
          "connectionString": "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;",          
          "SinkOptions":{
            "TableName": "ABCLogs",
            "autoCreateSqlTable":true
          }
        }
      },{
        "Name":"Console"

      }
    ]
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "SqlServer": "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;",
    "PostgreSQL": "server=host.9ihub.com;database=soft2_api_demo;uid=postgres;pwd=qq_112358;"
  }
}
```

Soft2ApiDemo.Infrastructure文件夹下Filters文件夹下MyGlobalExceptionFilter.c

```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Soft2ApiDemo.Infrastructure.Filters
{
    public class MyGlobalExceptionFilter : IAsyncExceptionFilter
    {
        private readonly IRepository<AppLogs> _logsRes;

        public MyGlobalExceptionFilter(IRepository<AppLogs> logsRes){
            
        }

        public Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.ExceptionHandled == false)
            {
                string msg = context.Exception.Message;
                context.Result =
                    new ContentResult {
                        Content = msg,
                        StatusCode = 200,
                        ContentType = "text/html;charset=utf-8"
                    };
            }
            context.ExceptionHandled = true; //异常已处理了

            return Task.CompletedTask;
        }
    }
}
```