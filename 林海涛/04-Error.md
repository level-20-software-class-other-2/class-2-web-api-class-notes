## 将引用添加进项目

> Could not load assembly 'Demo.Infrastructure'. Ensure it is referenced by the startup project 'Demo.Api'.

```js
dotnet add ..\Demo.Api\ reference .
```
> 已将引用“..\Demo.Infrastructure\Demo.Infrastructure.csproj”添加到项目。

## 命名空间报错

> error CS0234: 命名空间“Demo”中不存在类型或命名空间名“Domain”(是否缺少程序集引用?) [D:\WebApi\Frist\Demo.Infrastructure\Demo.Infrastructure.csproj]

```js
dotnet add . reference ..\Demo.Domain\
```

```js
dotnet restore //还原
```

> 涉及目标“_GenerateRestoreProjectPathWalk”的目标依赖项关系图中存在循环依赖项。 [D:\WebApi\Frist\Demo.Domain\Demo.Domain.csproj]
