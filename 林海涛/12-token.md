JWT最佳实践：

* 步骤一:在配置文件中设置一些配置 
* 步骤二：在UsersController中，新增加一个用户登录的方法，用于用户的登录，登录成功，返回token 
* 步骤三:真正生成token的，单独写一个帮助类

在配置文件中设置一些配置：

```json
"TokenParameter": {
    "secret": "0123456789ABCDEF", // 密钥
    "issuer": "WooZ", // 签发人
    "Audience":"Anyone", // 受众
    "accessExpiration": 120, // token的作用时间，单位为分钟，过期后需要重新获取
    "refreshExpiration": 1440 // 类似一个token的东西的作用时间，单位为分钟，用于重新获取token
  }
```

然后在Infrastructure中添加Parameter文件夹，在该文件夹中创建TokenParameter.cs 文件

```C#
namespace ApiDemo.Infrastructure.Parameter
{
    public class TokenParameter
    {
        //token的密钥，不能泄露，泄露意味着所有人都可以生成你的token并且访问你的服务或者接口
        public string Secret { get; set; }

        //发行人（可以是个人或组织）
        public string Issuer { get; set; }

        // 受众
        public string Audience { get; set; }

        //token的作用时间，单位为分钟，过期后需要重新获取
        public int AccessExpiration { get; set; }

        //类似一个token的东西的作用时间，单位为分钟，用于重新获取token
        public int RefreshExpiration { get; set; }
    }
}
```

在ApiDemo.Api 项目文件夹的启动项 Startup.cs 里注册验证器

```C#
// 注册验证器（使用何种配置来验证token）
services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(option =>{
                // 是否要求使用https
                option.RequireHttpsMetadata=false;
                // 是否保存token
                option.SaveToken=true;
                // 使用配置中间件，获得token的配置
                var tokenParameter=Configuration.GetSection("TokenParameter").Get<TokenParameter>();
                // 验证器的核心属性
                option.TokenValidationParameters=new TokenValidationParameters
                {
                    ValidateIssuerSigningKey=true,//要不要验证生成token的密钥
                    IssuerSigningKey=new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                    ValidateIssuer=true, // 要不要验证发行token的人（个人或者组织）
                    ValidIssuer=tokenParameter.Issuer,
                    ValidateAudience=false // 是否验证受众
                };
            });
```

再写有个帮助类TokenHelper.cs（要注意引用命名空间）

```c#
using ApiDemo.Infrastructure.Parameter;
using Micro.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using ApiDemo.Domain.Entity;
using System.Security.Claims;
using System.Text;
using System;
using ApiDemo.Infrastructure.Dto;

namespace ApiDemo.Infrastructure.Utility
{
   public static class TokenHelper
   {
        public static string GenerateToken(TokenParameter tokenParameter)//,Users users
        {
            var claims = new[]
            {
                // new Claim(users.Username,"admin"),
                new Claim("Name","admin"),
                new Claim("Roles","admin"),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var jwtToken = new JwtSecurityToken(
                tokenParameter.Issuer,
                null,
                claims,
                expires: DateTime.UtcNow.AddMinutes(tokenParameter.AccessExpiration),
                signingCredentials: credentials
            );
            var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            return token;
        }

        // 这里是刷新token的用法，没有数据验证、没有用户标识，也没有经过完美的测试，请勿直接用于生产环境，否则，后果自负
        public static RefreshTokenDto RefreshToken(TokenParameter tokenParameter, string oldToken)
        {
            //这儿是验证Token的代码
            var handler = new JwtSecurityTokenHandler();
            ClaimsPrincipal claim = handler.ValidateToken(oldToken, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false,
            }, out SecurityToken securityToken);
            var username = claim.Identity.Name;
            //这儿是生成Token的代码
            var token = GenerateToken(tokenParameter);
            //此处的RefreshToken是直接指定和字符串，在正式项目中，应该是一个可验证的加密串，如md5、hash等
            var refreshToken = "654321";

            var res = new RefreshTokenDto
            {
                AccessToken = token,
                RefreshToken = refreshToken
            };
            return res;
        }
   } 
}
```

控制器中的登入：

```C#
[AllowAnonymous]
[HttpGet, Route("token")]
public dynamic Login(UserCto model)
{
    var tokenParameter = _configuration.GetSection("TokenParameter").Get<TokenParameter>();
    var user = _usersRes.Table.Where(x => x.Username == model.Username && x.Password == model.Password).FirstOrDefault();
    if (user == null)
    {
        return new
        {
            Code = 1001,
            Data = user,
            Msg = "用户名或密码不正确，请核对后重试"
        };
    }

    var token = TokenHelper.GenerateToken(tokenParameter);

    var res = new
    {
        Code = 1000,
        Msg = "认证成功",
        Data = token
    };
    return res;
}
```

刷新token

```C#
// 获取新的token
[AllowAnonymous]
[HttpGet,Route("refresh/{token}")]
public string refreshToken(string token)
{
	var tokenParameter = _configuration.GetSection("TokenParameter").Get<TokenParameter>();
	var refreshToken=TokenHelper.RefreshToken(tokenParameter,token);
	return refreshToken.SerializeObject();
}
```