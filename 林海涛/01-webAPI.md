# webAPI

## 创建项目

dotnet new webapi -o 文件夹名称  ---- 创建文件夹并创建项目

dotnet new sln -n 文件名称  ---- 创建解决方案

dotnet sln add 项目名称  ---- 将项目添加到解决方案


## 简介
API即应用程序编程接。用来连接两个不同的系统,并使其中一方为另一方提供服务。

在Web应用当中，WebAPI也有同样的特性。它作为一个Web程序，向外提供了一些接口，这些接口的功能通常是对数据进行操作(获取/修改数据等)。能够被外部程序访问并调用。

# 关于gitee

git init  --- 初始化一个仓库

git status  --- 查看状态

git log  --- 查看本地缓存区

## 分支合并

git checkout 分支名  --- 创建一个分支

git checkout master  --- 切换成主分支

git merge 分支名  --- 合并分支