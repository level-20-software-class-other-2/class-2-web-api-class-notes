## 试试就逝世


Roles.cs
```
namespace Soft2ApiDemo.Domain.Entity
{
    // 角色
    public class Roles : BaseEntity
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        /// <value></value>
        public string Rolename { get; set; }
    }
}
```

UserRoles.cs
```
using System;

namespace Soft2ApiDemo.Domain.Entity
{
    /// <summary>
    /// 用户角色实体类型
    /// </summary>
    public class UserRoles : BaseEntity
    {
        /// <summary>
        ///用户Id
        /// </summary>
        /// <value></value>
        public Guid UserId { get; set; }

        /// <summary>
        ///角色Id
        /// </summary>
        /// <value></value>
        public Guid RoleId { get; set; }

        /// <summary>
        /// 用户实例
        /// </summary>
        /// <value></value>
        public Users User { get; set; }

        /// <summary>
        ///角色实例
        /// </summary>
        /// <value></value>
        public Roles Role { get; set; }
    }
}
```



Users.cs
```
namespace Soft2ApiDemo.Domain.Entity
{
    public class Users : BaseEntity
    {
        public string Username { get; set; }

        public string Password { get; set; }

    }
}
```


IRepositoryBase.cs
```
using System;
using System.Linq;

namespace Soft2ApiDemo.Domain.Repository
{
    /// <summary>
    /// CRUD接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepositoryBase<T>
    {
        T GetById(Guid id);

        IQueryable<T> Table { get; }

        void Add(T entity);

        void Update(T entity);

        void Delete(Guid id);
    }
}
```

BaseEntity.cs 
```
using System;

namespace Soft2ApiDemo.Domain
{
    // 所有实体类型的公共属性/字段
    public abstract class BaseEntity
    {
        // 主键
        public Guid Id { get; set; }

        // 标识是否启用
        public bool IsActived { get; set; }

        // 是否删除（伪删除、软删除）
        public bool IsDeleted { get; set; }

        // 创建者（的Id）
        public Guid CreatedBy { get; set; }

        // （最后）修改者（的Id）
        public Guid UpdatedBy { get; set; }

        // 记录的创建时间
        public DateTime CreatedAt { get; set; }

        // （最后）更新时间
        public DateTime UpdatedAt { get; set; }

        // 展示顺序，默认为0
        public int DisplayOrder { get; set; }

        // 备注
        public string Remarks { get; set; }
    }
}

```


 Soft2ApiDemoDbContext.cs
 ```
using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Domain.Entity;

namespace Soft2ApiDemo.Infrastructure.Db
{
    public class Soft2ApiDemoDbContext : DbContext
    {
        public Soft2ApiDemoDbContext(DbContextOptions options) :
            base(options)
        {
        }

        // private readonly string
        //     _conString = "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;";

        // protected override void OnConfiguring(DbContextOptionsBuilder builder)
        // {
        //     builder.UseSqlServer (_conString);
        // }

        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }

        public DbSet<UserRoles> UserRoles { get; set; }
    }
}

 ```

 RepositoryBase.cs 
 ```
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Domain;
using Soft2ApiDemo.Domain.Repository;
using Soft2ApiDemo.Infrastructure.Db;

namespace Soft2ApiDemo.Infrastructure.Repository
{
    /// <summary>
    /// CRUD接口的一个实现
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RepositoryBase<T> : IRepositoryBase<T> where T : BaseEntity
    {
        private readonly Soft2ApiDemoDbContext _db;

        private DbSet<T> _table;

        public RepositoryBase(Soft2ApiDemoDbContext db)
        {
            _db = db;
            _table = _db.Set<T>();
        }

        public T GetById(Guid id)
        {
            var entity = _table.Where(x => x.Id == id).FirstOrDefault();
            return entity;
        }

        public IQueryable<T> Table
        {
            get
            {
                if (_table == null)
                {
                    _table = _db.Set<T>();
                }
                return _table.AsNoTracking().AsQueryable<T>();
            }
        }

        public void Add(T entity)
        {
            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedAt = DateTime.Now;
            entity.UpdatedAt = DateTime.Now;
            entity.DisplayOrder = 0;
            _db.Add (entity);
            _db.SaveChanges();
        }

        public void Update(T entity)
        {
            entity.UpdatedAt = DateTime.Now;
            _db.Update (entity);
            _db.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                _db.Remove(entity);
                _db.SaveChanges();
            }
        }
    }
}

 ```


 Startup.cs
 ```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Soft2ApiDemo.Infrastructure.Db;
using Soft2ApiDemo.Domain.Repository;
using Soft2ApiDemo.Infrastructure.Repository;

namespace Soft2ApiDemo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var conString = Configuration.GetConnectionString("SqlServer");

            // IoC 依赖倒转
            // DI  依赖注入

            // DI分两大步，第一步叫注册，注册服务接口和具体实现到容器
            // 第二步，其实就是真正在使用的时候，注入服务到构造函数、属性、特性
            services
                .AddDbContext<Soft2ApiDemoDbContext>(options =>
                {
                    options.UseSqlServer(conString);
                });

            services.AddScoped(typeof(IRepositoryBase<>),typeof(RepositoryBase<>));

            
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }
    }
}

 ```

 WeatherForecast.cs

 ```
 using System;

namespace Soft2ApiDemo.Api
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}

 ```


 Program.cs
 ```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Soft2ApiDemo.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}


 ```

 Program.cs
 ```
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using System.IO;

namespace Soft2ApiDemo.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var environment =
                Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var config =
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json",
                    optional: true,
                    reloadOnChange: true)
                    .AddJsonFile($"appsettings.{environment}.json",
                    optional: true,
                    reloadOnChange: true)
                    .Build();

            Log.Logger =
                new LoggerConfiguration()
                    .ReadFrom
                    .Configuration(config)
                    .CreateLogger();

            try
            {
                Log.Information("开始Web宿主");
                CreateHostBuilder(args).Build().Run();
                // return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                // return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
            // CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
                .CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

 ```

 ```
 {
  "Serilog": {
    "Using": [
      "Serilog.Sinks.MSSqlServer"
    ],
    "MinimumLevel": "Debug",
    "WriteTo": [
      {
        "Name": "MSSqlServer",
        "Args": {
          "connectionString": "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;",
          "tableName": "ABCLogs"
        }
      }
    ]
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "SqlServer": "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;",
    "PostgreSQL": "server=host.9ihub.com;database=soft2_api_demo;uid=postgres;pwd=qq_112358;"
  }
}
 ```