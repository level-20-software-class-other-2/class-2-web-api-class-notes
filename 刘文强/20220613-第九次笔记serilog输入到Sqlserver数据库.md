## serilog输入到Sqlserver数据库的补充:使用配置文件配置日志到数据库SQLserver

appsettings.json文件中的配置如下:

```json
{

    "Serilog": {
        "MinimumLevel": "Information",
        "WriteTo": [{
            "Name": "MSSqlServer",//什么数据库
            "Args": {
                "connectionString": "server=.;database=galaxy;uid=sa;pwd=123456;",//数据库连接字符串
                "tableName": "Log",//日志表名
                "autoCreateSqlTable": true//是否创建表
            }
        }]
    },
    //以上为serilog配置文件

    "AllowedHosts": "*",
    "ConnectionStrings": {
        "SqlServer": "server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;",
        "PostgreSQL": "server=host.9ihub.com;database=soft2_api_demo;uid=postgres;pwd=qq_112358;"
    }
}
```



Program.cs 文件代码如下:

```cs
public static void Main(string[] args)
{
    IConfigurationRoot configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json",optional: false, reloadOnChange: true).Build();

    Log.Logger = new LoggerConfiguration()
        //读取配置文件
        .ReadFrom.Configuration(configuration)

        //---该部分是控制台打印的信息
        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        .Enrich
        .FromLogContext()
        .WriteTo
        .Console()
        //---
        .CreateLogger();

        try
            {
                Log.Information("开始Web宿主");
                CreateHostBuilder(args).Build().Run();
                // return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                // return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
}


public static IHostBuilder CreateHostBuilder(string[] args) =>
    Host
        .CreateDefaultBuilder(args)
        .UseSerilog()
        .ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder.UseStartup<Startup>();
        });
```

效果如下:

![](./imgs/20220612171614.png)
