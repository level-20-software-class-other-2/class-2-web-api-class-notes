# 2022-06-12-审计日志

### 什么是审计日志?

“审计跟踪（也叫审计日志）是与安全相关的按照时间顺序的记录，记录集或者记录源，它们提供了活动序列的文档证据，这些活动序列可以在任何时间影响一个特定的操作，步骤或其他”

### 审计日志的作用是什么

* 快速定位问题耗时及性能情况。
* 记录调用时环境信息：如浏览器、参数等。

### 定义审计日志信息

在 SoftApiDemo.Domain 里的 Entity 文件夹创建日志表 AuditInfo.cs ：

```C#
using System;

namespace SoftDemo.Domain.Entity
{
    public class AuditInfo:BaseEntity
    {
        /// <summary>
        /// 调用参数
        /// </summary>
        /// <value></value>
        public string Parameters { get; set; }
        /// <summary>
        /// 浏览信息
        /// </summary>
        /// <value></value>
        public string BrowserInfo { get; set; }
        /// <summary>
        /// 客户端信息
        /// </summary>
        /// <value></value>
        public string ClientName { get; set; }
        /// <summary>
        /// 客户端Ip地址
        /// </summary>
        /// <value></value>
        public string ClientIpAddress { get; set; }
        /// <summary>
        /// 执行耗时
        /// </summary>
        /// <value></value>
        public int ExecutionDuration { get; set; }
        /// <summary>
        /// 执行时间
        /// </summary>
        /// <value></value>
        public DateTime ExecutionTime { get; set; }
        /// <summary>
        /// 返回内容
        /// </summary>
        /// <value></value>
        public string ReturnValue { get; set; }
        /// <summary>
        /// 异常对象
        /// </summary>
        /// <value></value>
        public string Exception { get; set; }
        /// <summary>
        /// 方法名称
        /// </summary>
        /// <value></value>
        public string MethodName  { get; set; }
        /// <summary>
        /// 服务名称
        /// </summary>
        /// <value></value>
        public string ServiceName  { get; set; }
        /// <summary>
        /// 调用者信息
        /// </summary>
        /// <value></value>
        public string UserInfo   { get; set; }
        /// <summary>
        /// 自定义数据
        /// </summary>
        /// <value></value>
        public string CustomData   { get; set; }
    }
}
```

### 实现审计日志过滤器

该内容为实现审计日志功能主要逻辑，通过过滤器获取当前执行控制器、方法判断是否需要记录审计日志；其他请求参数、客户端ip等相关基本信息组成审计日志对象，并记录调用时间。　

这个内容我是放在基础设施文件夹 SoftApiDemo.Infrastructure 的 Filters

```C#
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using SoftDemo.Domain.Entity;
// using SoftDemo.Infrastructure.Repository;
using SoftDemo.Domain.Repository;

namespace SoftDemo.Infrastructure.Filters
{
    public class AuditLogActionFilter : IAsyncActionFilter
    {
        // private readonly IActionFilte
        private readonly IRepositoryBase<AuditInfo> _auditLogService;

        public AuditLogActionFilter(IRepositoryBase<AuditInfo> auditLogService)
        {
            this._auditLogService=auditLogService;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // 判断是否写日志
            if (!ShouldSaveAudit(context))
            {
                await next();
                return;
            }
            //接口Type
            var type = (context.ActionDescriptor as ControllerActionDescriptor).ControllerTypeInfo.AsType();
            //方法信息
            var method = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;
            //方法参数
            var arguments = context.ActionArguments;
            //开始计时
            var stopwatch = Stopwatch.StartNew();

             var auditInfo = new AuditInfo
            {
                UserInfo = "admin",
                ServiceName = type != null ? type.FullName: "",
                MethodName = method.Name,
                ////请求参数转Json
                Parameters = JsonConvert.SerializeObject(arguments),
                ExecutionTime = DateTime.Now,
                BrowserInfo = context.HttpContext.Request.Headers["User-Agent"].ToString(),
                ClientIpAddress = context.HttpContext.Connection.RemoteIpAddress.ToString(),
                //ClientName = _clientInfoProvider.ComputerName.TruncateWithPostfix(EntityDefault.FieldsLength100),
                // Id = Guid.NewGuid().ToString()
            };

            ActionExecutedContext result = null;
            try
            {
                result = await next();
                if (result.Exception != null && !result.ExceptionHandled)
                {
                    auditInfo.Exception = result.Exception.ToString();
                }
            }
            catch (Exception ex)
            {
                auditInfo.Exception = ex.ToString();
                throw;
            }
            finally
            {
                stopwatch.Stop();
                auditInfo.ExecutionDuration = Convert.ToInt32(stopwatch.Elapsed.TotalMilliseconds);

                if (result != null)
                {
                    switch (result.Result)
                    {
                        case ObjectResult objectResult:
                            auditInfo.ReturnValue = JsonConvert.SerializeObject(objectResult.Value);
                            break;

                        case JsonResult jsonResult:
                            auditInfo.ReturnValue = JsonConvert.SerializeObject(jsonResult.Value);
                            break;

                        case ContentResult contentResult:
                            auditInfo.ReturnValue = contentResult.Content;
                            break;
                    }
                }
                Console.WriteLine(auditInfo.ToString());
                //保存审计日志
                await _auditLogService.InsertAsync(auditInfo);
            }
            
        }

        private bool ShouldSaveAudit(ActionExecutingContext context)
        {
            if (!(context.ActionDescriptor is ControllerActionDescriptor))
                return false;
            var methodInfo = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;

            if (methodInfo == null)
            {
                return false;
            }

            if (!methodInfo.IsPublic)
            {
                return false;
            }

            return true;
        }
    }
}
```

### 设置启动项

在 SoftApiDemo.Api 中的 Startup.cs 文件里中的ConfigureServices()方法里的 services.AddControllers(); 替换成

```C#
services.AddControllers(options =>
{
    options.Filters.Add(typeof(AuditLogActionFilter));
});
```

完整的 Startup.cs 文件

```C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using SoftDemo.Infrastructure.Filters;
using SoftDemo.Infrastructure.Db;
using SoftDemo.Domain.Repository;
using SoftDemo.Infrastructure.Repository;

namespace SoftDemo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var conString = Configuration.GetConnectionString("SqlServer");
            /*
                IoC(Inversion of Control) 控制反转
                DI(Dependency injection) 依赖注入


                DI分两大步，第一步叫注册，注册服务接口和具体实现到容器
                第二步，其实就是真正在使用的时候，注入服务到构造函数、属性、特性
            */

            // 注册数据库上下文到容器,审计日志存储
            services.AddDbContext<SoftDemoDbContext>(options =>
            {
                options.UseSqlServer(conString);
            });
            // 注册对数据库的基本操作服务到容器
            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));

            // services.AddControllers();


            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(AuditLogActionFilter));
            });
            
            //  services.AddScoped<UserManager>();
            // services.AddSwaggerGen(c =>
            // {
            //     c.SwaggerDoc("v1", new OpenApiInfo { Title = "SoftDemo.Api", Version = "v1" });
            // });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // if (env.IsDevelopment())
            // {
            //     app.UseDeveloperExceptionPage();
            //     app.UseSwagger();
            //     // app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SoftDemo.Api v1"));
            // }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

```