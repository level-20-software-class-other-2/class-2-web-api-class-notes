# webApi
## 新建项目
+ 1.dotnet new webapi --no-https  =>在当前文件夹下新建一个项目
+ 2.dotnet new webapi -o (项目文件名称) --no-https  =>在当前文件夹下新建一个文件夹，项目在新建的文件夹下
+ 3.dotnet new sln -n (文件名称)
+ 4.dotnet new classlib -o (文件名.Domain) 	领域驱动
+ 5.dotnet sln add (项目文件名) =>将项目添加到解决方案中

## gitee
+ git init （初始化一个仓库）
+ git status （查看状态）
+ git log （查看本地的暂存区）
### 分支合并
+ git checkout (分支名) 比如修复bug的分支
+ 解决问题后 上传 git add . git commit -m"name"
+ git checkout master  （选择主分支，就可以看到合并过来的分支了） 