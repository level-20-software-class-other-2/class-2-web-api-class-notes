3 今天是说序列化
序列化是在serilog程序可以使用后创建的。

第一步: 在.infrastructure文件中创建一个Uitls文件夹并创建JSonHelper.cs文件

文件内容如下:
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace soft2webapi.infrastructure.Utils
{
    public static class JSonHelper
    {
        public static string SerializeObject(this object obj)
        {
            var options =
                new JsonSerializerOptions {
                    Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
                    WriteIndented = true
                };
            return JsonSerializer.Serialize(obj, options);
        }
    }
}
```
+ 第二步:

创建成功后在控制器中使用

引用程序集、
```
using soft2webapi.infrastructure.Utils;
```
+ 实例化
```
    private readonly ILogger<Users> _logger;

    public UsersController(IRepositoryBase<Users> users,ILogger<Users> logger){
         _users= users;
        _logger=logger;
    }
```
+ 最后使用日志输出
```
_logger.LogInformation(list.SerializeObject());
```