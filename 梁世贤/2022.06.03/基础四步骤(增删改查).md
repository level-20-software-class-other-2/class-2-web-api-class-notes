# 增删改查
在增删改查后端功能实现之前，可以对之前数据迁移做一些调整目的是为了数据库上下文实例注入。

变更数据迁移方式其他不变

第一步： 先在soft2webapi.infrastructure文件中的Db文件夹中的soft2webapiDbContext(就是Db文件) 作以下修改

添加：

       // 此处为Db文件名   可以依照自己的文件修改
public soft2webapiDbContext(DbContextOptions options):base(options)
{
}
如果报错表达式需要返回值的说明方法命名与你的文件命名不一致修改即可。

第二步： 在soft2webapi.Api文件中的StartUp.cs文件中

注意：我使用的postgres数据库连接,sqlseerver数据库更改对应代码即可，下载程序也是。

添加:

//不想在json文件中写脚本可以用这个方式获取数据库连接字符串也行。
string connect = "server=www.galaxyeye.top;database=galaxy;uid=postgres;pwd=113;";


//json中写对应的数据库连接字符串脚本可以用以下方式获取。
var conString = Configuration.GetConnectionString("PostgreSQL");

services.AddDbContext<soft2webapiDbContext>(options =>
{
                    //此处可以进行更改。
    options.UseNpgsql(conString);
});
json文件数据库连接脚本模板：


  "ConnectionStrings": {
    "SqlServer":"server=.;database=Soft2ApiDemo;uid=sa;pwd=123456;",
    "PostgreSQL":"server=xxxxx;database=galaxy;uid=postgres;pwd=xxx;"
  }
文件引用:

using soft2webapi.infrastructure.Db;
using Microsoft.EntityFrameworkCore;//下载对应数据库连接方式
完成之后同样连接数据库

dotnet ef database update -s ..\soft2webapi.Api\
以上部分修改老版本的连接数据库的方式是为了DI 依赖注入为接下增删改查做准备。

Repository文件夹
第一步:

在.Domain文件夹中插件Repository文件夹并创建IRepositoryBase.cs文件

内容为:


using System.Linq;  //IQueryable<>需要
using System;       //Guid需要
namespace soft2webapi.Domain.Repository
{
                                  //<T>的作用可以理解为占位符,对应不同的表
    public interface IRepositoryBase<T>
    {
        T GetById(Guid Id);//根据Id获取指定数据

        IQueryable<T> Table{get;}//集成查询

        void Add(T entity);//增添数据

        void Update(T entity);//更新数据

        void Delete(Guid Id);//删除数据(伪删除)
    }
}
在.infrastructure文件夹中插件Repository文件夹并创建RepositoryBase.cs文件

内容为:

using System;
using System.Linq;
using soft2webapi.Domain;
using System.Collections.Generic;
using soft2webapi.Domain.Repository;
using Microsoft.EntityFrameworkCore;
using soft2webapi.infrastructure.Db;

namespace soft2webapi.infrastructure.Repository{

    public class RepositoryBase<T> : IRepositoryBase<T> where T :BaseEntity{

        private readonly soft2webapiDbContext _db;//定义数据库上下文实例
        private DbSet<T> _table;                  //定义表实例

        public RepositoryBase(soft2webapiDbContext db)//文件初始化时
        {
            _db=db;//实例化数据库上下文
            _table = _db.Set<T>();//实例化表
        }

        public T GetById(Guid Id){

            var entity = _table.Where(x=>x.Id==Id).FirstOrDefault();
            return entity;

        }

        public IQueryable<T> Table {
            get{
                if(_table==null){
                    _table=_db.Set<T>();
                }
                return _table.AsNoTracking().AsQueryable<T>();
                //AsNoTracking 称之为获取不带变动跟踪的实体查询
            }
        }

        public void Add(T entity){

            entity.IsActived=true;
            entity.IsDeleted=false;
            entity.CreatedAt=DateTime.Now;
            entity.UpdatedAt=DateTime.Now;
            entity.DisplayOrder=0;

            _db.Add(entity);
            _db.SaveChanges();

            }

        public void Update(T entity){

            entity.UpdatedAt=DateTime.Now;

            _db.Update(entity);
            _db.SaveChanges();

        }

        public void Delete(Guid Id){
            var entity = GetById(Id);

            if(entity!=null){

                entity.IsDeleted=true;//伪删除

                _db.Update(entity);
                _db.SaveChanges();
            }

        }

    }
        
}
第二步:

在.Api文件夹中的StartUp.cs文件

添加:

    services.AddScoped(typeof(IRepositoryBase<>),typeof(RepositoryBase<>));
以上就是Repository文件夹及依赖注入的过程。

增删改查的实现
可以在Controllers文件夹中创建一个UsersController.cs文件

内容:

using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using soft2webapi.Domain.Entity;
using System.Collections.Generic;
using soft2webapi.Domain.Repository;


namespace soft2webapi.Api.Controllers{
    
    [ApiController]
    [Route("[controller]")]//这两个作为路由使用
    public class UsersController : ControllerBase
    {

        private readonly IRepositoryBase<Users> _users;//创建Users表实例

        public UsersController(IRepositoryBase<Users> users){//通过构造函数获取实例,依赖注入的使用。
            _users= users;
        }

        [HttpGet]//get请求
        public IEnumerable<Users> Index(){//插入默认数据,返回Users表全部数据。
            _users.Add(new Users{
                UserName="cheems",
                Password="dream"
            });
            var list = _users.Table.ToArray();
        
            return list;
        }
        
        public class UserType{//新增更新的数据类型
            public string UserName{set;get;}
            public string Password{set;get;}
        }
        [HttpPost]//post请求
        public Users Add(UserType data){
            var user = new Users{
                UserName=data.UserName,
                Password=data.Password
            };
            _users.Add(user);
            return user;
        }

        [HttpPut]//put请求
        [Route("{id}")]//有id传值
        public Users Update(Guid id ,UserType data){
            var entity = _users.GetById(id);
            if(entity!=null){
                entity.UserName=data.UserName;
                entity.Password=data.Password;
                _users.Update(entity);
            }
            return entity;
        }
        
        [HttpDelete]//delete请求
        [Route("{id}")]//有id传值
        public dynamic  Delete(Guid id){
            var entity = _users.GetById(id);

            if(entity!=null){
                entity.IsDeleted=true;
                _users.Update(entity);
            }
            return new {
                code=1000,
                msg="success"
            };
        }
    }
}
关于显示指定列表的方法

    [HttpGet]
    public dynamic Index(){

        var list = _users.Table.Select(t=> new {t.UserName,t.Password,t.Id}).ToArray();

        return list;
    }