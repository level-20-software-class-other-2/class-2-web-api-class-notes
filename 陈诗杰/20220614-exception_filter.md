# 过滤器
在某些特定的时候， 我们希望将一些错误给写入到对应的数据库中， 在 .Net 中我们可以使用异常过滤器来实现这个功能，   
因为电脑快没电了， 所以写的简单一点， 后面随缘完善。

---
首先， 我们需要实现 `IAsyncExceptionFilter` 接口中的 `OnExceptionAsync` 方法，在默认情况下所有的异常都会触发这个函数:
``` cs
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Demo.Infrastructure.Filter
{
    public class GlobalExcepionFilter : IAsyncExceptionFilter
    {
        public GlobalExcepionFilter() { }
        public Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.ExceptionHandled == false)
            {
                // insert logic here
                context.Result = new ContentResult
                {
                    Content = context.Exception.Message,
                    StatusCode = 200,
                    ContentType = "text/html;charset=utf-8"
                };
            }

            context.ExceptionHandled = true;
            return Task.CompletedTask;
        }
    }
}
```
然后， 我们需要在 `Program.cs` 中引入我们实现的类:
``` cs
//找到 builder.Services.AddController, 将其修改为下面那样
builder.Services.AddControllers(options => options.Filters.Add(new GlobalExcepionFilter()));
```
如果是 .Net 6.0 以下的版本， 那么这行代码会在 `Startup.cs` 中， 还有值得注意的一点是， .Net 6.0 会自动（全局）   
引入一些命名空间， 所有有些在低版本需要手动引入的东西这里没用列出来， 因为电脑真的要没电了。