# REST
表述性状态转移 (REST) 是一种用于生成 Web 服务的体系结构样式。   
REST 请求是通过 HTTP 发出的。 它们使用 Web 浏览器用于检索网页和将数据发送到服务器的相同 HTTP 谓词。 谓词如下：
+ GET     从 Web 服务获取数据
+ POST    向 Web 服务创建一些数据
+ PUT     更新 Web 服务中已有的一些数据
+ DELETE  删除 Web 服务中的数据。
+ PATCH   通过描述有关如何修改项的一组说明，更新 Web 服务上的数据项， 通常情况下用不上。

遵循 REST 规范的 `Web API` 被称为 `RESTful API`。   
它们通过以下方法进行定义：
+ 一个 Base URL
+ HTTP 请求方法， 也就是上面说的那五种。
+ 数据的媒体类型， 比如 `JSON`
  
举个通俗一点的例子， 我们在学习使用 `Node.JS` 开发前后端分离的项目时，   
开发的后端其实就是一个 `RESTful API`。 与传统的 Web 服务不同，`Web API` 接受请求并返回的数据，   
并不是 HTML、CSS 或者 JS, 举个例子， 有一个 API 其接受 GET 请求后， 会响应一个 JSON 字符串，   
在浏览器中其显示的效果在没有安装插件的情况下会非常的糟糕 —— 没有经过任何的处理， 就这样一个字符串   
从浏览器窗口的最左边显示到最右边， 在快溢出屏幕时换行， 如果没有什么层级结构还好， 要是有，那就会是地狱:
``` json
{
  "code": 1000,
  "msg": "success",
  "data": [
    {
      "id": 1,
      "product_name": "p1",
      "price": "50.12",
      "stock": 30,
      "supplier": "s1"
    },
    {
      "id": 2,
      "product_name": "p2",
      "price": "50.12",
      "stock": 30,
      "supplier": "s2"
    },
    {
      "id": 3,
      "product_name": "p3",
      "price": "50.12",
      "stock": 20,
      "supplier": "s3"
    }
  ]
}
```
试着将这么一个玩意给拉平，没有任何缩进或者换行， 那么这玩意会变成这样:
``` json
{"code":1000,"msg":"success","data":[{"id":1,"product_name":"p1","price":"50.12","stock":30,"supplier":"s1"},{"id":2,"product_name":"p2","price":"50.12","stock":30,"supplier":"s2"},{"id":3,"product_name":"p3","price":"50.12","stock":20,"supplier":"s3"}]}
```
如果这里面在多那么一点点嵌套， 那么实际效果将会惨不忍睹， 而且在通常情况下， 用浏览器去访问 `Web API`，   
在不写代码的情况下只能发送 `get` 请求， 所以事实上就算通过浏览器扩展解决了显示效果的问题，也会遇上其他问题，    
比如没法发送 GET 请求以外的请求之类的问题，总不能做一个 api 还得在写一个用于发送请求的工具来测试吧。   

## RELP
在使用一个工具之前， 肯定得安装它:
``` shell
# 全局安装 httprepl 工具
dotnet tool install -g Microsoft.dotnet-httprepl
```
运行:
``` shell
# 只是运行
httprepl
# 运行并连接到一个 Web API 服务
httprepl url
```
也可以通过 `connetc` 命令来连接服务（需要运行在 httprepl 中）
``` shell
connetc url # command not found
httprepl # 启动 httprepl
connetc url # 连接到对应的 api
# 上面的两行等价于
httprepl url
```
值得注意的是就算要连接的对象不在线， 或者说不存在， 命令行也会变为连接的状态，   
不过在这种情况下执行与 api 交互的指令是不会具有任何效果的， 因为目标根本就不存在嘛。

---
### 使用 repl 测试 web api
例子以默认情况下的项目为例， 也就是通过 `dotnet new webapi` 生成的， 不做任何修改的项目。
我们可以通过 ls 命令来查看该 api 服务下所具有的终结点（官网文档是这么写的， 实际上就是控制器)：
``` shell
# output
.                 []
WeatherForecast   [GET]
```
然后我们可以切换到该控制器下， 然后执行对应的命令， 比如发起一次 get 请求:
``` shell
# 切换到该控制器下
cd WeatherForecast
# 发送一个 get 请求
get
```
当然，我们也可以不切换， 直接对目标发起一个 get 请求，就像这样:
``` shell
get WeatherForecast
```
这两者是等价的，然后控制台就会打印出服务器的响应， 然后现在，来试试发送一个 POST 请求，   
通常情况下， 我们会得到一个错误:
``` shell
The default editor must be configured using the command `pref set editor.command.default "{commandLine}"`.
```
而post或者put请求通常需要携带一些数据， 这些数据可能是 JSON, 也可能是 XML，   
不过这都不重要，重要的是我们需要一个编辑工具来编辑这些对象， 而这在默认情况下，   
httprepl 不会自带默认的编辑工具，不过我们可以手动设置它：
``` shell
pref set editor.command.default "{commandLine}"
```
其中 `commandLine` 为一个可执行程序的绝对路径， 这里以 vim 举例，   
如果我们希望将 vim 设置为 httprepl 的默认编辑器， 我们可以将 `commandLine`   
替换为 vim 的绝对路径， 如果不知道 vim 在哪， 那么可以通过以下命令查看:
``` shell
whereis vim
# output
vim: /usr/bin/vim /usr/bin/vim.tiny /usr/bin/vim.basic /etc/vim /usr/share/vim /usr/share/man/man1/vim.1.gz
```
然后控制台就会输出所有带有 vim 的路径， 我们很早就知道 /usr/bin 下存放的是二进制文件， 选择其中一个的其中一个设置为默认编辑器即可，   
当然如果想要使用其他的编辑器也是一样的， 不过 `whereis` 在默认情况下只有 Windows 是没有该指令的， 如果使用的是 Windows, 那么就找到该编辑器的快捷方式，  
然后右键打开文件位置， 然后将 `commandLine` 替换为该 `exe` 文件的绝对路径即可， 如果想要退出 httprepl, 只需要执行 `exit` 即可。