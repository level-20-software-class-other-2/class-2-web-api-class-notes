# Serilog日志记录到Sqlserver

## 1.首先来一大串的安装
如下模块安装在ApiDemo.Infrastructure项目里
```
dotnet add package Package Serilog

dotnet add package Serilog.AspNetCore

dotnet add package Serilog.Settings.Configuration

dotnet add package Serilog.Sinks.MSSqlServer
```

## 2.修改```Program.cs```文件
```C#
using System;
using Serilog;
using Serilog.Events;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ApiDemo.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var environment =
                Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var config =
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json",
                    optional: true,
                    reloadOnChange: true)
                    .AddJsonFile($"appsettings.{environment}.json",
                    optional: true,
                    reloadOnChange: true)
                    .Build();

            Log.Logger =
                new LoggerConfiguration()
                    .ReadFrom
                    .Configuration(config)
                    .CreateLogger();
            try
            {
                Log.Information("Starting web host");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
            .CreateDefaultBuilder(args)
            .UseSerilog((context, services, configuration) =>
                    configuration
                        .ReadFrom
                        .Configuration(context.Configuration)
                        .ReadFrom
                        .Services(services)
                        .Enrich
                        .FromLogContext()
                        .WriteTo
                        .Console())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
```

## 3.修改```appsettings.json```文件
```json
{
  "Serilog": {
    "MinimumLevel": "Information",
    "WriteTo": [
      {
        "Name": "MSSqlServer",
        "Args": {
          "connectionString": "server=.;database=ApiDemoDb;uid=sa;pwd=123456;",
          "tableName": "Logs",
          "autoCreateSqlTable": true
        }
      }
    ]
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "SqlServer":"server=.;database=ApiDemoDb;uid=sa;pwd=123456;"
  }
}
```

## 4.运行项目，执行api.http中的get请求
![](./imgs/logs.png)