# 2022-06-17-JWT token认证

## 什么是JWT?（Json Web Token）

JWT（全称：Json Web Token）是一个开放标准(RFC 7519)，它定义了一种紧凑的、自包含的方式，用于作为JSON对象在各方之间安全地传输信息。该信息可以被验证和信任，因为它是数字签名的。简单点说就是一种认证机制，让后台知道该请求是来自于受信的客户端。

简单说一个流程：

1. 用户使用账号、密码登录应用，登录的请求发送到Authentication Server。
2. Authentication Server进行用户验证，然后创建JWT字符串返回给客户端。
3. 客户端请求接口时，在请求头带上JWT。
4. Application Server验证JWT合法性，如果合法则继续调用应用接口返回结果。

可以看出与token方式有一些不同的地方，就是不需要依赖redis，用户信息存储在客户端。所以关键在于生成JWT，和解析JWT这两个地方。

## JWT的数据结构

JWT一般是这样一个字符串，分为三个部分，以"."隔开：

```text
xxxxx.yyyyy.zzzzz
```

### Header

其中 x 部分代表头（Header）它是一个描述JWT元数据的Json对象，通常如下所示。

```text
{
  "alg": "HS256",
  "typ": "JWT",
  "cty": "JWT"
}
```

alg属性表示签名使用的算法，默认为HMAC SHA256（写为HS256），typ属性表示令牌的类型，JWT令牌统一写为JWT。

最后，使用Base64 URL算法将上述JSON对象转换为字符串保存。

### Payload

其次 y 部分代表有效载荷（Payload）也是一个Json对象，除了包含需要传递的数据，还有七个默认的字段供选择。

分别是，iss：发行人、exp：到期时间、sub：主题、aud：用户、nbf：在此之前不可用、iat：发布时间、jti：JWT ID用于标识该JWT。

如果自定义字段，可以这样定义：

```text
{z
  "Name": "admin",
  "Roles": "admin",
  "exp": 1655529727,
  "iss": "WooZ"
}
```

需要注意的是，默认情况下JWT是未加密的，任何人都可以解读其内容，因此如果一些敏感信息不要存放在此，以防信息泄露。

JSON对象也使用Base64 URL算法转换为字符串保存。

### **Signature**

JWT第三部分（也就是 z 部分）是签名。是这样生成的，首先需要指定一个secret，该secret仅仅保存在服务器中，保证不能让其他用户知道。然后使用Header指定的算法对Header和Payload进行计算，然后就得出一个签名哈希。也就是Signature。

那么Application Server如何进行验证呢？可以利用JWT前两段，用同一套哈希算法和同一个secret计算一个签名值，然后把计算出来的签名值和收到的JWT第三段比较，如果相同则认证通过。

### JWT的优点

- json格式的通用性，所以JWT可以跨语言支持，比如Java、JavaScript、PHP、Node等等。
- 可以利用Payload存储一些非敏感的信息。
- 便于传输，JWT结构简单，字节占用小。
- 不需要在服务端保存会话信息，易于应用的扩展。

## 如何实现呢？

JWT最佳实践：

* 步骤一:在配置文件中设置一些配置 
* 步骤二：在UsersController中，新增加一个用户登录的方法，用于用户的登录，登录成功，返回token 
* 步骤三:真正生成token的，单独写一个帮助类

在配置文件中设置一些配置：

```json
"TokenParameter": {
    "secret": "0123456789ABCDEF", // 密钥
    "issuer": "WooZ", // 签发人
    "Audience":"Anyone", // 受众
    "accessExpiration": 120, // token的作用时间，单位为分钟，过期后需要重新获取
    "refreshExpiration": 1440 // 类似一个token的东西的作用时间，单位为分钟，用于重新获取token
  }
```

然后在Infrastructure中添加Parameter文件夹，在该文件夹中创建TokenParameter.cs 文件

```C#
namespace SoftApiDemo.Infrastructure.Parameter
{
    public class TokenParameter
    {
        //token的密钥，不能泄露，泄露意味着所有人都可以生成你的token并且访问你的服务或者接口
        public string Secret { get; set; }

        //发行人（可以是个人或组织）
        public string Issuer { get; set; }

        // 受众
        public string Audience { get; set; }

        //token的作用时间，单位为分钟，过期后需要重新获取
        public int AccessExpiration { get; set; }

        //类似一个token的东西的作用时间，单位为分钟，用于重新获取token
        public int RefreshExpiration { get; set; }
    }
}
```

在SoftApiDemo.Api 项目文件夹的启动项 Startup.cs 里注册验证器

```C#
// 注册验证器（使用何种配置来验证token）
services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(option =>{
                // 是否要求使用https
                option.RequireHttpsMetadata=false;
                // 是否保存token
                option.SaveToken=true;
                // 使用配置中间件，获得token的配置
                var tokenParameter=Configuration.GetSection("TokenParameter").Get<TokenParameter>();
                // 验证器的核心属性
                option.TokenValidationParameters=new TokenValidationParameters
                {
                    ValidateIssuerSigningKey=true,//要不要验证生成token的密钥
                    IssuerSigningKey=new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                    ValidateIssuer=true, // 要不要验证发行token的人（个人或者组织）
                    ValidIssuer=tokenParameter.Issuer,
                    ValidateAudience=false // 是否验证受众
                };
            });
```



再写有个帮助类TokenHelper.cs（要注意引用命名空间）

```c#
using SoftApiDemo.Infrastructure.Parameter;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using SoftApiDemo.Domain.Entity;
using System.Security.Claims;
using System.Text;
using System;
using Soft2ApiDemo.Infrastructure.Dto;

namespace SoftApiDemo.Infrastructure.Utility
{
   public static class TokenHelper
   {
        public static string GenerateToken(TokenParameter tokenParameter)//,Users users
        {
            var claims = new[]
            {
                // new Claim(users.Username,"admin"),
                new Claim("Name","admin"),
                new Claim("Roles","admin"),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var jwtToken = new JwtSecurityToken(
                tokenParameter.Issuer,
                null,
                claims,
                expires: DateTime.UtcNow.AddMinutes(tokenParameter.AccessExpiration),
                signingCredentials: credentials
            );
            var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            return token;
        }

        // 这里是刷新token的用法，没有数据验证、没有用户标识，也没有经过完美的测试，请勿直接用于生产环境，否则，后果自负
        public static RefreshTokenDto RefreshToken(TokenParameter tokenParameter, string oldToken)
        {
            //这儿是验证Token的代码
            var handler = new JwtSecurityTokenHandler();
            ClaimsPrincipal claim = handler.ValidateToken(oldToken, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false,
            }, out SecurityToken securityToken);
            var username = claim.Identity.Name;
            //这儿是生成Token的代码
            var token = GenerateToken(tokenParameter);
            //此处的RefreshToken是直接指定和字符串，在正式项目中，应该是一个可验证的加密串，如md5、hash等
            var refreshToken = "654321";

            var res = new RefreshTokenDto
            {
                AccessToken = token,
                RefreshToken = refreshToken
            };
            return res;
        }
   } 
}
```

控制器中的登入：

```C#
[AllowAnonymous]
[HttpGet, Route("token")]
public dynamic Login(UserCto model)
{
    var tokenParameter = _configuration.GetSection("TokenParameter").Get<TokenParameter>();
    var user = _usersRes.Table.Where(x => x.Username == model.Username && x.Password == model.Password).FirstOrDefault();
    if (user == null)
    {
        return new
        {
            Code = 1001,
            Data = user,
            Msg = "用户名或密码不正确，请核对后重试"
        };
    }

    var token = TokenHelper.GenerateToken(tokenParameter);

    var res = new
    {
        Code = 1000,
        Msg = "认证成功",
        Data = token
    };
    return res;
}
```

刷新token

```C#
// 获取新的token
[AllowAnonymous]
[HttpGet,Route("refresh/{token}")]
public string refreshToken(string token)
{
	var tokenParameter = _configuration.GetSection("TokenParameter").Get<TokenParameter>();
	var refreshToken=TokenHelper.RefreshToken(tokenParameter,token);
	return refreshToken.SerializeObject();
}
```

