# 2022-06-10-Serilog日志输出到SqlServer

在基础设施文件加安装 Serilog.Sinks.MSSqlServer

```
dotnet add package Serilog.Sinks.MSSqlServer
```

配置程序入口文件 Program.cs

```C#
var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"); // Development
var config =new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
    		   .AddJsonFile("appsettings.json",optional: true,reloadOnChange: true)
    		   .AddJsonFile($"appsettings.{environment}.json",optional: true,reloadOnChange: true)
                .Build();
Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(config).CreateLogger();
//需要引用Microsoft.Extensions.Configuration
```

配置公共配置文件 appsettings.json

- `connectionString` 数据库连接字符串
- `schemaName`　　数据库所有者，默认`dbo`
- `tableName` 记录日志的表名
- `autoCreateSqlTable` 是否自动创建表，如果设置为`ture`，则在`Serilog`启动时检测数据库是否有对应的表，没有则创建
- `columnOptions` 日志表中的列定义
- `restrictedToMinimumLevel` 记录日志的最小`level`
- `batchPostingLimit` 单次批量处理中提交的最大日志数量
- `period` 进行批量提交的间隔
- `formatProvider` 提供特定的格式化处理，https://github.com/serilog/serilog/wiki/Formatting-Output#format-providers

`Serilog`为我们定义了一套标准列，默认情况下会生成如下列，当然我们也可以自定义列

- `StandardColumn.Id` 自增Id
- `StandardColumn.Message` 日志内容
- `StandardColumn.MessageTemplate` 日志模板
- `StandardColumn.Level` 等级
- `StandardColumn.TimeStamp` 记录时间
- `StandardColumn.Exception` 异常信息
- `StandardColumn.Properties` 日志事件属性值

```json
{
  "Serilog": {
    "Using": [ "Serilog.Sinks.MSSqlServer"],
    "MinimumLevel": {
      "Default": "Information",
      "Override": {
        "Microsoft": "Information"
      }
    },
    "WriteTo": [
      {
        "Name": "Console",
        "Args": {
          "restrictedToMinimumLevel": "Debug"
        } 
      },
      {
        "Name": "MSSqlServer",
        "Args": {
          "connectionString": "server=.;database=SoftDemo;uid=sa;pwd=123456;",
          "sinkOptionsSection": {
            "tableName": "Logs",
            "AutoCreateSqlTable": true
          },
          "restrictedToMinimumLevel": "Debug"
        }
      }
    ],
    "Enrich": [ "WithMachineName", "WithThreadId" ],
    "Properties": {
      "Application": "Soft1ApiDemo"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "SqlServer": "server=.;database=SoftDemo;uid=sa;pwd=123456;"
  }
}
```

## 课堂扩展

### 肉鸡（受黑客远程控制的电脑）

肉鸡也称[傀儡机](https://baike.baidu.com/item/傀儡机/10413334)，是指可以被黑客[远程控制](https://baike.baidu.com/item/远程控制/934368)的机器。比如用"灰鸽子"等诱导客户点击或者电脑被黑客攻破或用户电脑有漏洞被种植了木马，黑客可以随意操纵它并利用它做任何事情。

肉鸡通常被用作[DDOS](https://baike.baidu.com/item/DDOS)攻击。可以是各种系统，如windows、[linux](https://baike.baidu.com/item/linux/27050)、[unix](https://baike.baidu.com/item/unix/219943)等，更可以是一家公司、企业、学校甚至是政府军队的[服务器](https://baike.baidu.com/item/服务器/100571)。

### 火腿（业余无线电爱好者）

业余无线电通信的英语名字是“Amateur Radio”，符合国际电信联盟组织ITU定义的业余无线电爱好者是“Radio Amateur”，在世界上又普遍被称为“HAM”。由于“HAM”一词，在英汉词典中，被解释为“火腿；业余无线电爱好者；拙劣的表演者”，所以“火腿”又成了业余无线电通信爱好者们的另一个名字（顺便解释一下所谓“拙劣的表演者”，不过是业余无线电爱好者均比较谦虚，自认有关无线电方面的知识、技能尚不及职业的专家而已）。

### 限流

什么是限流呢？限流是限制到达系统的并发请求数量，保证系统能够正常响应部分用户请求，而对于超过限制的流量，则通过拒绝服务的方式保证整体系统的可用性。

根据限流作用范围，可以分为 **单机限流和分布式限流** ；根据限流方式，又分为 **计数器、滑动窗口、漏桶和令牌桶限流**