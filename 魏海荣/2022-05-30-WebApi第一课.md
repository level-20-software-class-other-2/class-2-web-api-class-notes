## 2022-05-30-WebApi第一课

## RESTful（自己网上了解的）

和前后端分离一起，经常被提到的一个名词就是Restful(Representational State Transfer)，(懵jp g
其实RESTful是一种设计架构风格，类似于家装服饰:北欧风/日式/韩版
主要特点:

* 通过HTTP(注意HTTP协议的无状态State)请求获取资源(URI)
* 通过定义HTTP请求中的方法(method)表示(仅仅是语义上的，没有强制性，就好比HTML5的语义标签一样)服务端资源的操作(状态转化StateTransfer):
  * GET:获取
  * POST:新建
  * PUT:更新
  * DELETE:删除
    在某些特定情形下，区分GET/POST就已经有些“迷茫”了，再添加PUT和DELETE，概念进一步混淆
* 数据的传递形式包括:JSON/XML/HTML片段(表现层Representation)

RESTful不仅仅适用干B/S架构:随着前后端分离，区分B/S和C/S架构没有意义了

## 创建WebApi项目

```
// -o 表示创建在指定根目录
dotnet new webapi --no-https -o XXXX

// 创建解决方案 -n 表示 --name 文件名
dotnet new sln -n XXXX

// 把解决方案添加到指定项目，解决方案是整个实现目的或目标的项目集成,然后逐一解决最后达成目的或目标
dotnet sln add XXXX[项目文件夹名称]
```

## git分支

```git
//创建一个分支 
git checkout -b fixbug[分支名]

//然后就可以在这个分支下提交文件啦
git add .
git commit -m "XXX"

// 返回主分支master
git checkout master

// 在master分支中合并分支
git merge fixbug
```

