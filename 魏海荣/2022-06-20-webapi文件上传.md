# 2022-06-20-webapi文件上传

关于文件上传详细请看官方文档[asp.net core文件上传]([在 ASP.NET Core 中上传文件 | Microsoft Docs](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/models/file-uploads?view=aspnetcore-6.0))

### 先实现简单的文件上传：

写一个文件上传的控制器

```C#
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SoftApiDemo.Domain.Entity;
using SoftApiDemo.Domain.Repository;
using SoftApiDemo.Infrastructure.Utility;

namespace SoftApiDemo.Api.Controllers
{
    [ApiController]
    [Consumes("application/json", "multipart/form-data")]//此处为新增
    [Route("[controller]")]
    public class FilesController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        public FilesController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost, Route("Upload")]
        public async Task<string> FileSave(List<IFormFile> files)
        {
            // 消除跨域问题
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");

            // （通过配置文件获取到的）设置上传文件
            var uploadFilePath = _configuration.GetValue<string>("UploadFilespath");
            // 当前路径（完整路径）
            var currentPath = Directory.GetCurrentDirectory();

            // 用于容纳要返回的相对地址
            var list = new List<string>();

            foreach (var formFile in files)
            {
                var fileName = files[0].FileName;
                if (formFile.Length > 0)
                {
                    // 返回一个随机的文件夹名或文件名。
                    var filePathName = Guid.NewGuid().ToString("N");
                    // 扩展名
                    var extenName = formFile.FileName.Substring(formFile.FileName.IndexOf("."));

                    // 新的文件名（包含扩展名）
                    var newFileName = string.Format("{0}{1}", filePathName, extenName);

                    // 根据年月日来创建文件夹
                    var now = DateTime.Now;
                    // 相对路径（不包含当前路径、不包含新文件路径）
                    var relativePath = Path.Combine(uploadFilePath, now.Year.ToString(), now.Month.ToString(), now.Day.ToString());

                    //拼凑出完整的存放路径（完整路径 + 相对路径）
                    var saveFilePath = Path.Combine(currentPath, relativePath);

                    //如果目录不存在就创建
                    if (!Directory.Exists(saveFilePath))
                    {
                        Directory.CreateDirectory(saveFilePath);
                    }

                    // 写入硬盘
                    // 创建或覆盖指定路径中的文件。 
                    using (var stream = System.IO.File.Create(Path.Combine(saveFilePath, newFileName)))
                    {
                        // 异步地将上传文件的内容复制到目标流。 
                        await formFile.CopyToAsync(stream);
                        // 清除此fs流的缓冲区，使得所有缓冲数据都写入到文件中
                        await stream.FlushAsync();
                    }
                    // 返回的文件路径（一般为相对路径）
                    list.Add(Path.Combine(relativePath, newFileName).Replace("\\", "/"));
                }
            }
			// 这里返回文件路径
            return list.SerializeObject();
        }
    }
}
```

### 文件上传数据库：

先创建一个实体类

```C#
using System;

namespace SoftApiDemo.Domain.Entity
{
    public class FilesPocket:BaseEntity
    {
        /// 原始文件名
        public string OldFileName { get; set; }
        /// 当前文件名
        public string NewFileName { get; set; }
        /// 扩展名
        public string ExtenName { get; set; }
        /// 文件类型
        public string FileType { get; set; }
    }
}
```

然后再文件控制器中做一些小文章就可以了

```C#
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SoftApiDemo.Domain.Entity;
using SoftApiDemo.Domain.Repository;
using SoftApiDemo.Infrastructure.Utility;

namespace SoftApiDemo.Api.Controllers
{
    [ApiController]
    [Consumes("application/json", "multipart/form-data")]//此处为新增
    [Route("[controller]")]
    public class FilesController : ControllerBase
    {
		// 引入FilesPocket仓储
        private readonly IRepositoryBase<FilesPocket> _filesPocket;
        private readonly IConfiguration _configuration;

        public FilesController(IConfiguration configuration,IRepositoryBase<FilesPocket> filesPocket)
        {
            _configuration = configuration;
            _filesPocket = filesPocket;
        }

        [HttpPost, Route("Upload")]
        public async Task<string> FileSave(List<IFormFile> files)
        {
            // 消除跨域问题
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");

            
            // long size = files.Sum(f => f.Length);

            // （通过配置文件获取到的）设置上传文件
            var uploadFilePath = _configuration.GetValue<string>("UploadFilespath");
            // 当前路径（完整路径）
            var currentPath = Directory.GetCurrentDirectory();

            // 用于容纳要返回的相对地址
            var list = new List<string>();

            foreach (var formFile in files)
            {
                var fileName = files[0].FileName;
                if (formFile.Length > 0)
                {
                    // 返回一个随机的文件夹名或文件名。
                    var filePathName = Guid.NewGuid().ToString("N");
                    // 扩展名
                    var extenName = formFile.FileName.Substring(formFile.FileName.IndexOf("."));

                    // 新的文件名（包含扩展名）
                    var newFileName = string.Format("{0}{1}", filePathName, extenName);

                    // 根据年月日来创建文件夹
                    var now = DateTime.Now;
                    // 相对路径（不包含当前路径、不包含新文件路径）
                    var relativePath = Path.Combine(uploadFilePath, now.Year.ToString(), now.Month.ToString(), now.Day.ToString());

                    //拼凑出完整的存放路径（完整路径 + 相对路径）
                    var saveFilePath = Path.Combine(currentPath, relativePath);

                    //如果目录不存在就创建
                    if (!Directory.Exists(saveFilePath))
                    {
                        Directory.CreateDirectory(saveFilePath);
                    }

                    // 写入硬盘
                    // 创建或覆盖指定路径中的文件。 
                    using (var stream = System.IO.File.Create(Path.Combine(saveFilePath, newFileName)))
                    {
                        // 异步地将上传文件的内容复制到目标流。 
                        await formFile.CopyToAsync(stream);
                        // 清除此fs流的缓冲区，使得所有缓冲数据都写入到文件中
                        await stream.FlushAsync();
                    }
                    // 返回的文件路径（一般为相对路径）
                    list.Add(Path.Combine(relativePath, newFileName).Replace("\\", "/"));
                    
                    // 插入数据库
                    _filesPocket.Insert(new FilesPocket{
                        // 原始文件名称
                        OldFileName=formFile.FileName,
                        // 当前文件名称
                        NewFileName=newFileName,
                        // 扩展名
                        ExtenName=extenName,
                        // 文件类型（我为了偷懒才这样搞的生产环境中文件类型应该是要写应该判断方法）
                        FileType=extenName.Substring(1)
                    });
                    
                }
            }
			// 这里返回文件路径
            return list.SerializeObject();
        }
    }
}
```

简单来所文件上传就是这样了

