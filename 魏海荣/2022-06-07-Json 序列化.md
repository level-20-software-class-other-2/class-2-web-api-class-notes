# 2022-06-07-Json 序列化

### System.Text.Json 自定义字符编码

在 SoftApiDemo.Infrastructure创建一个Utility文件夹 写一个 JSonHelper.cs 文件

```C#
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace SoftDemo.infrastructure.Utility
{
    public static class JSonHelper
    {
        public static string SerializeObject(this object obj)
        {
            var options =new JsonSerializerOptions 
                {
                    Encoder =JavaScriptEncoder.Create(UnicodeRanges.BasicLatin,UnicodeRanges.Cyrillic),
                    WriteIndented = true
                };
            var jsonString = JsonSerializer.Serialize(obj, options);
            return jsonString;
        }
    }
}

```

