# 2022-06-06-Serilog日志

### 返回指定字段

```C#
public IEnumerable<dynamic> Index(){
    //通过select指定显示列
    var list =  _userRes.Table.Select(x=>new {
        x.Id,
        x.Username,
        x.Password
    }).ToArray();
    var uuu = new List<dynamic>{
        new {
            name="wd",
            children=new List<dynamic>{
                new {kname="af"}
            }
        }
    };
    //可以通过new 来新增实例
    return list;
}
```

### 控制器获取指定参数

```C#
[HttpGet]
[Route("{id}/friend")]
//id后的多路径
public dynamic getfind(Guid id){
    var list = new{
        code=1000,
        msg="200",
        data=new {
            Name="2333"
        }
    };
    return list;
}
```

## Serilog 日志

根据上一次的项目在基础设施文件 SoftDemo.Infrastructure上安装 Serilog.AspNetCore 软件包

```
dotnet add package Serilog.AspNetCore
```

第二步在应用程序SoftDemo.Api文件中的 Program.cs 文件 配置 Serilog 以及添加 UseSerilog() 到通用主机 CreateHostBuilder()

```C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace SoftDemo.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger =
                new LoggerConfiguration()
                    .MinimumLevel
                    .Debug()
                    .WriteTo
                    .Console()
                    .WriteTo
                    .File("logs/myapp.txt",
                    rollingInterval: RollingInterval.Day)
                    .CreateLogger();
            try
            {
                Log.Information("启动网络主机");
                CreateHostBuilder(args).Build().Run();
                // return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "主机意外终止");
                // return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
                .CreateDefaultBuilder(args)
                .UseSerilog() // <-- 这是添加的
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

```

然后把 appsettings.json 中的 Logging 删除替换成 Serilog

 appsettings.json 文件

```json
{
  "Serilog": {
    "MinimumLevel": {
      "Default": "Information",
      "Override": {
        "Microsoft": "Warning",
        "System": "Warning"
      }
    },
    "WriteTo": [
      { "Name": "Console" },
      {
        "Name": "File",
        "Args": { "path": "C:\\LogFiles\\log.txt" }
      }
    ],
    "Enrich": [ "FromLogContext", "WithMachineName", "WithThreadId" ]
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "SqlServer":"server=.;database=SoftDemo;uid=sa;pwd=123456;"
  }
}

```

在控制器中使用只要借用依赖注入机制就可以了：

```C#
[ApiController]
[Route("[controller]")]
public class UsersController:ControllerBase
{
    /// <summary>
    /// 依赖于用户的仓储。
    /// </summary>
    private readonly IRepositoryBase<Users> _usersRes;
    // ⾸先.NETCore通过继承 ILogger接⼝实现了它⾃⼰的⽇志记录。通过借助依赖注⼊机制，它可以很容易地使⽤。
    private readonly ILogger<UsersController> _logger; // <-- 添加的
    /// <summary>
    /// 构造，初始化。
    /// </summary>
    /// <param name="usersRes"></param>
    public UsersController(IRepositoryBase<Users> usersRes,ILogger<UsersController> logger)
    {
        this._usersRes=usersRes;
        this._logger=logger; // <-- 添加的
    }
    
    [HttpGet]
    // [Route("[controller/users]")]
    /// <summary>
    /// 请求用户数据
    /// </summary>
    /// <returns>返回一个列表</returns>
    public IEnumerable<Users> Index(){
        // 三种日志
        _logger.LogInformation("Info 日志"); 
        _logger.LogDebug("Debug 日志");
        _logger.LogError(new System.IO.IOException(),"Error 日志");
        var list=_usersRes.Table.ToArray();
        return list;
	}
}
```



