
需要下载的包并需要在Program.cs文件中引入对应的包

```
dotnet add package Serilog.Sinks.MSSqlServer
```
在.Api文件夹的Program.cs文件中写入或替换以下代码

```js
public static void Main(string[] args)
{

    var connectionString = @"server=.;database=soft;uid=sa;pwd=123456;";//连接数据库
    string tableNameA = "Logs";//日志表名会在数据库生成这个表，然后控制台的输出日志就会插入到

//连接数据库字符串 输入日志的表名 autoCreateSqlTable为true 时 会自动创建日志表
    Log.Logger = new LoggerConfiguration().WriteTo.MSSqlServer(connectionString,tableNameA, autoCreateSqlTable: true)
    //控制台输出内容
    .MinimumLevel
    .Override("Microsoft", LogEventLevel.Information)
    .Enrich
    .FromLogContext()
    .WriteTo
    .Console()
    .CreateLogger();

    try
    {
        Log.Information("开始Web宿主");
        CreateHostBuilder(args).Build().Run();
        // return 0;
    }
    catch (Exception ex)
    {
        Log.Fatal(ex, "Host terminated unexpectedly");
        // return 1;
    }
    finally
    {
        Log.CloseAndFlush();
    }

}

//日志配置只增加.UseSerilog()，


public static IHostBuilder CreateHostBuilder(string[] args) =>
    Host
        .CreateDefaultBuilder(args)
        .UseSerilog()//---->增加此行
        .ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder.UseStartup<Startup>();
        });

```

